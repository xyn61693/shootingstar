﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class TxtMessage : MonoBehaviour
{
    private Text m_Text;

    public Color normal;
    public Color crit;
    public Color recover;
    
    private void Awake()
    {
        m_Text = GetComponentInChildren<Text>();
    }

    public void SetDamage(int d,bool isCrit,Vector3 pos)
    {
        transform.localPosition = pos;
        m_Text.color = isCrit ? crit : normal;
        m_Text.text = d.ToString();
        Fade();
    }

    public void SetRecover(int r,Vector3 pos)
    {
        transform.localPosition = pos;
        m_Text.color = recover;
        m_Text.text = r.ToString();
        Fade();
    }

    public void Custom(string content, Vector3 pos,Color color)
    {
        transform.localPosition = pos;
        m_Text.color = color;
        m_Text.text = content;
        Fade();
    }
    
    private void Fade()
    {
/*
        m_Text.DOFade(0f, 1f);
        transform.DOMove(transform.localPosition + new Vector3(0, 5f, 0), 1.5f)
            .OnComplete(() => PoolManager.instance.Recycle(gameObject));
            */
        m_FadeTime = 0.5f;
        m_FadeAlpha = m_Text.color.a / (m_FadeTime / Time.deltaTime);
        m_FadePos = new Vector3(0, 3f / (m_FadeTime / Time.deltaTime),0);
    }

    private float m_FadeTime;
    private float m_FadeAlpha;
    private Vector3 m_FadePos;
    
    private void Update()
    {
        if (m_FadeTime > 0)
        {
            m_FadeTime -= Time.deltaTime;

            m_Text.color = FadeColor(m_Text.color);
            transform.localPosition += m_FadePos;
        }
        else if (m_FadeTime < 0.01f)
        {
            m_FadeTime = 0f;
            PoolManager.instance.Recycle(gameObject,true);
        }
    }

    private Color FadeColor(Color c)
    {
        return new Color(c.r, c.g, c.b, c.a - m_FadeAlpha);
    }
}
