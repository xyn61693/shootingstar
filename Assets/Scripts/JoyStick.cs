﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Experimental.U2D;
using UnityEngine.UI;

public class JoyStick : MonoBehaviour,IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private static JoyStick m_Instance;
	
    public static JoyStick instance
    {
        get { return m_Instance; }
    }
    
    public RectTransform handler;
    public RectTransform background;

    public float axisLength = 100f;

    private bool m_Enable = true;
    
    private Vector2 m_CenterPos;
    private Vector3 m_OrgPos;
    
    private CanvasGroup m_CanvasGroup;
    
    private float m_Strength;
    private Vector3 m_Input;

    private float m_KeyboradStrength;
    private float m_AxisH;
    private float m_AxisV;
    
    private void Awake()
    {
        m_Instance = this;
        background.sizeDelta = Vector2.one * axisLength * 2 /*+ handler.sizeDelta*/;
        m_CanvasGroup = GetComponent<CanvasGroup>();
        m_OrgPos = background.position;
    }
    
#if UNITY_EDITOR
    //Keyboard Input
    private void Update()
    {
        if (m_Enable && m_Strength == 0f)
        {
            m_AxisH = Input.GetAxis("Horizontal");
            if (Mathf.Abs(m_AxisH) <= 0.2f) m_AxisH = 0f;
            
            m_AxisV = Input.GetAxis("Vertical");
            if (Mathf.Abs(m_AxisV) <= 0.2f) m_AxisV = 0f;
            
            m_Input = new Vector3(m_AxisH, 0, m_AxisV);
            m_KeyboradStrength = m_Input.sqrMagnitude < 1f ? m_Input.sqrMagnitude : 1f;
            if (m_KeyboradStrength <= 0.1f) m_Input = Vector3.zero;
        }
    }
#endif
    
    public void OnDrag(PointerEventData eventData)
    {
        if(!m_Enable) return; 
        var strength = Vector2.Distance(eventData.position, m_CenterPos) / axisLength;
        m_Strength = strength < 1f ? strength : 1f;
        handler.position = strength < 1f
            ? eventData.position
            : m_CenterPos + (eventData.position - m_CenterPos).normalized * axisLength;

        var input = (handler.position - new Vector3(m_CenterPos.x,m_CenterPos.y)).normalized;
        m_Input = new Vector3(input.x, 0, input.y);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(!m_Enable) return; 
        m_CanvasGroup.DOFade(1f, 0.1f);
        m_CenterPos = eventData.position;
        background.position = m_CenterPos;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        m_Input = Vector3.zero;
        m_Strength = 0f;
        m_CanvasGroup.DOFade(0.3f, 0.1f);
        handler.DOMove(m_OrgPos, 0.1f);
        background.DOMove(m_OrgPos, 0.1f);
    }

    public void SetJoyStickEnable(bool enable)
    {
        m_Enable = enable;
        if (!enable)
        {
            m_Input = Vector3.zero;
            m_Strength = 0f;
            m_CanvasGroup.DOFade(0.3f, 0.1f);
            handler.DOMove(m_OrgPos, 0.1f);
            background.DOMove(m_OrgPos, 0.1f);
            
        }
    }

    private float m_AbsX;
    private float m_AbsZ;
    private float m_FixX;
    private float m_FixZ;
    
    public Vector3 GetAxisInput()
    {
        if (m_Input == Vector3.zero) return m_Input;
        m_AbsX = Mathf.Abs(m_Input.x);
        m_AbsZ = Mathf.Abs(m_Input.z);
        m_FixX =  m_AbsX / (m_AbsX + m_AbsZ) * ((m_AbsX + m_AbsZ) - 1);
        m_FixZ = m_AbsZ / (m_AbsX + m_AbsZ) *  ((m_AbsX + m_AbsZ) - 1);
        m_Input.x += m_Input.x < 0 ? m_FixX : -m_FixX;
        m_Input.z += m_Input.z < 0 ? m_FixZ : -m_FixZ;
        return m_Input;
    }

    public float GetInputStrength()
    {
        return m_Strength == 0f ? m_KeyboradStrength : m_Strength;
    }
}
