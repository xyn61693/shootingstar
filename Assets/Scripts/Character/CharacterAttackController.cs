﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CharacterAttackController : MonoBehaviour
{
    public bool attackWhileMove;
    public AnimationClip attackClip;
    
    public float attackSpeed;
    private float m_BasisAttackSpeed;
    public int damage;
    private int m_BasisDamage;
    
    private float m_AttackInterval;
    private float m_AttackTime;

    #region AttackCount
    private int m_AttackCount;
    private int m_CritNeedAttackCount;
    private bool m_IsAddAttackCount;
    #endregion
    
    #region Crit
    public float critRate;
    public int critDamage;
    #endregion
    
    private CharacterInputController m_InputController;
    protected CharacterHealthyController healthy;
    
    private void Awake()
    {
        m_InputController = GetComponent<CharacterInputController>();
        healthy = GetComponent<CharacterHealthyController>();
        m_AttackInterval = attackClip.length;
        m_BasisAttackSpeed = attackSpeed;
        m_BasisDamage = damage;

        OnInit();
        
        m_BtnSkill = UIManager.instance.Find("Skill").GetComponent<Button>();
        m_ImgSkill = m_BtnSkill.GetComponent<Image>();
        m_BtnSkill.onClick.AddListener(Skill);
    }
    
    private void Start()
    {
        if(attackWhileMove) StartCoroutine(AttackWhileMove());
    }

    public void Update()
    {
        #if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Skill();
        }
        #endif

        if (m_IsUpdateRange)
        {
            m_AllAi = CharacterManager.instance.enemyList;
            for (int i = 0; i < m_AllAi.Count; i++)
            {
                m_Distance = Vector3.Distance(m_AllAi[i].transform.position, transform.position);
                
                if (m_InRangeSpeedCut > 0)
                {
                    UpdateSpeedCutRange(m_Distance,m_AllAi[i]);
                }
                /*
                 Update Other Range Effect
                 */
            }
        }
    }
    
    private IEnumerator AttackWhileMove()
    {
         yield return new WaitForSeconds(m_AttackInterval);
         if (m_InputController.GetState() == CharacterInputController.MoveState.OnMove)
         {
             OnAttack();
         }
         StartCoroutine(AttackWhileMove());
    }
    
    public void Kill()
    {
        if(attackWhileMove) StopCoroutine(AttackWhileMove());
        OnKill();
        enabled = false;
    }

    public void IncreaseAttackSpeed(float percent)
    {
        attackSpeed += m_BasisAttackSpeed * percent;
        var state = m_InputController.GetState();
        if (state == CharacterInputController.MoveState.End)
        {
//            GetComponent<Animator>().Play("Attack");
            GetComponent<Animator>().speed = attackSpeed;
        }
    }
    
    public void IncreaseDamage(float percent)
    {
        damage += (int) (m_BasisDamage * percent);
    }

    public void IncreaseCrit(float rate,int damagePercent)
    {
        critRate += rate;
        critDamage += damagePercent;
    }

    protected virtual void OnInit() { }
    
    protected virtual void OnAttack()
    {
        AddAttackCount();
    }
    
    protected virtual void OnKill() { }

    protected bool IsCrit()
    {
        //第7次必定暴击
        if (m_IsAddAttackCount && m_AttackCount % m_CritNeedAttackCount == 0) return true;
        
        return Random.Range(0f, 1f) <= critRate;
    }

    private void AddAttackCount()
    {
        if(!m_IsAddAttackCount) return;
        m_AttackCount++;
    }

    public void SetAttackCount(bool enable,int critNeedAttackCount)
    {
        m_IsAddAttackCount = enable;
        m_CritNeedAttackCount = critNeedAttackCount;
    }

    #region HitBonus

    protected int crowbarBonus;
    protected int axeBonus;
    protected int crownBonus;
    protected int capeBonus;
    
    public void AddCrowbarBonus(int increase)
    {
        crowbarBonus += increase;
    }

    public void AddAxeBonus(int increase)
    {
        axeBonus += increase;
    }

    public void AddCrownBonus(int increase)
    {
        crownBonus += increase;
    }
    
    public void AddCapeBonus(int increase)
    {
        capeBonus += increase;
    }
    
    #endregion 
    
    #region HitCb
    
    private int m_HitRecoverHp = 0;
    private int m_CritRecoverHp = 0;

    private int m_SpeedCut = 0;
    
    protected void HitEnemyCb(AiController ai,bool isCrit)
    {
        if (m_HitRecoverHp > 0)
        {
            healthy.Recover(m_HitRecoverHp);
        }

        if (isCrit && m_CritRecoverHp > 0)
        {
            healthy.Recover(m_CritRecoverHp);
        }

        if (m_SpeedCut > 0)
        {
            var isCut = RandomUtils.IsInRange(15);
            if (!isCut) return;
            var move = ai.GetComponents<AiMove>();
            for (int i = 0; i < move.Length; i++)
            {
                move[i].SetSpeedCut(m_SpeedCut,4f);
            }
        }
    }

    public void IncreaseHitEnemyRecoverHp(int increase)
    {
        m_HitRecoverHp += increase;
    }

    public void IncreaseCritEnemyRecoverHp(int increase)
    {
        m_CritRecoverHp += increase;
    }

    public void IncreaseSpeedCut(int increase)
    {
        m_SpeedCut += increase;
    }
    
    #endregion

    #region KillCb
    
    private int m_KillRecoverHp = 0;
    
    protected void KillEnemyCb(AiController ai)
    {
        if (m_KillRecoverHp > 0)
        {
            healthy.Recover(m_KillRecoverHp);
        }
    }

    public void IncreaseKillRecoverHp(int hp)
    {
        m_KillRecoverHp += hp;
    }
    #endregion

    #region Skill
    
    [TitleGroup("Skill",order:2)] public int skillCount;
    [TitleGroup("Skill")] public float skillInterval;
    [TitleGroup("Skill")] public float skillDelay;
    private bool m_IsOnSkillDelay = false;
    private bool m_IsSkillEnable = true;

    public void SetSkill(bool enable)
    {
        m_IsSkillEnable = enable;
    }
    
    private Button m_BtnSkill;
    private Image m_ImgSkill;
    
    public void Skill()
    {
        if (!m_IsOnSkillDelay && m_IsSkillEnable && CharacterManager.instance.player)
        {
            OnSkillStart();
            StartCoroutine(SkillCoroutine());
        }
    }
    
    private IEnumerator SkillCoroutine()
    {
        m_IsOnSkillDelay = true;
        m_ImgSkill.color = Color.gray;
        for (int i = 0; i < skillCount; i++)
        {
            if(CharacterManager.instance.player == null) yield break;
            yield return OnSkill();
            yield return new WaitForSeconds(skillInterval);
        }
        OnSkillEnd();
        yield return new WaitForSeconds(skillDelay);
        m_IsOnSkillDelay = false;
        m_ImgSkill.color = Color.white;
    }

    protected virtual IEnumerator OnSkill()
    {
        yield return null;
    }
    
    protected virtual void OnSkillStart() { }
    
    protected virtual void OnSkillEnd() { }
    
    #endregion

    #region RangeAttack

    private bool m_IsUpdateRange;

    private List<AiController> m_AllAi;

    private float m_Distance;

    #region SpeedCutRange
        
        private int m_InRangeSpeedCut = -1;
    
        private float m_SpeedCutRange;
    
        private List<AiController> m_SpeedCutRangeController = new List<AiController>();

        private AiMove[] m_TmpAiMove;
        
        public void InitInRangeSpeedCut(int increase,float range)
        {
            if (!m_IsUpdateRange) m_IsUpdateRange = true;
            m_InRangeSpeedCut = increase;
            m_SpeedCutRange = range;
            var effect = PoolManager.instance.GetCell("Vfx_SpeedCutRange");
            effect.transform.SetParent(transform);
            effect.transform.position = transform.position;
        }
    
        private void EnterInRangeSpeedCut(AiController ai)
        {
            m_TmpAiMove = ai.GetComponents<AiMove>();
            for (int i = 0; i < m_TmpAiMove.Length; i++)
            {
                m_TmpAiMove[i].SetSpeedCut(m_InRangeSpeedCut);
            }
        }
    
        private void ExitInRangeSpeedCut(AiController ai)
        {
            m_TmpAiMove = ai.GetComponents<AiMove>();
            for (int i = 0; i < m_TmpAiMove.Length; i++)
            {
                m_TmpAiMove[i].EndSpeedCut(m_InRangeSpeedCut);
            }
        }
        
        private void UpdateSpeedCutRange(float dis, AiController ai)
        {
            if (dis <= m_SpeedCutRange)
            {
                if (!m_SpeedCutRangeController.Contains(ai))
                {
                    m_SpeedCutRangeController.Add(ai);
                    EnterInRangeSpeedCut(ai);
                }
            }
            else
            {
                if (m_SpeedCutRangeController.Contains(ai))
                {
                    m_SpeedCutRangeController.Remove(ai);
                    ExitInRangeSpeedCut(ai);
                }
            }
        }
        
        public void IncreaseInRangeSpeedCut(int increase)
        {
            //Remove effect which is working
            for (int i = 0; i < m_SpeedCutRangeController.Count; i++)
            {
                ExitInRangeSpeedCut(m_SpeedCutRangeController[i]);
            }
            
            m_InRangeSpeedCut += increase;
            
            //Update Effect
            for (int i = 0; i < m_SpeedCutRangeController.Count; i++)
            {
                EnterInRangeSpeedCut(m_SpeedCutRangeController[i]);
            }
        }
    
        public void IncreaseSpeedCutRange(float range)
        {
            m_SpeedCutRange += range;
        }
        
    #endregion
    
    #endregion

    public void OnAttackTest()
    {
        OnAttack();
    }
}
    