﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;

public class ArcherAttack : CharacterAttackController
{
    public GameObject arrow;
    
    
    public Transform attackPosFront;
    /*
    public Transform attackPosLeft;
    public Transform attackPosRight;
    public Transform attackPosBack;

    public int attackFrontCount;
    
    public bool isAttackSide;
    [ShowIf("isAttackSide")]
    public int attackSideCount;
    
    public bool isAttackBack;
    [ShowIf("isAttackBack")]
    public int attackBackCount;
    */
    
    public float arrowSpeed = 1f;

    protected override void OnInit()
    {
    }

    protected override void OnAttack()
    {
        base.OnAttack();
        CreateCell(attackPosFront);
        /*
        StartCoroutine(Shooting(attackFrontCount,attackPosFront));
        if (isAttackBack)
        {
            StartCoroutine(Shooting(attackBackCount,attackPosBack));
        }
        if (isAttackSide)
        {
            StartCoroutine(Shooting(attackSideCount,attackPosLeft));
            StartCoroutine(Shooting(attackSideCount,attackPosRight));
        }
        */
    }

    /*
    private IEnumerator Shooting(int count,Transform pos)
    {
        for (int i = 0; i < count; i++)
        {
            CreateCell(pos);
            yield return new WaitForSeconds(0.1f);
        }
    }
    */
    private void CreateCell(Transform t)
    {
        var a = PoolManager.instance.GetCell(arrow);
        //UnityEngine.Profiling.Profiler.BeginSample("CreateCell");
        a.transform.SetPositionAndRotation(t.position,t.rotation);
        
        var bouns = damage;
        if (crownBonus > 0 && healthy.GetHpPercent() >= 1f)
        {
            bouns += Mathf.CeilToInt((damage * crownBonus) * 0.01f);
        }
        
        if (capeBonus > 0 && healthy.GetHpPercent() <= 0.3f)
        {
            bouns += Mathf.CeilToInt((damage * capeBonus) * 0.01f);
        }
        var isCrit = IsCrit();
        var d = isCrit ? Mathf.CeilToInt(bouns * critDamage * 0.01f) : bouns;
        //UnityEngine.Profiling.Profiler.EndSample();
        a.GetComponent<AttackCell>().Init(arrowSpeed,d,isCrit,crowbarBonus,axeBonus,HitEnemyCb,KillEnemyCb);
    }
    
    protected override void OnKill()
    {
        /*
        StopCoroutine(Shooting(attackFrontCount,attackPosFront));
        StopCoroutine(Shooting(attackBackCount,attackPosBack));
        StopCoroutine(Shooting(attackSideCount,attackPosLeft));
        StopCoroutine(Shooting(attackSideCount,attackPosRight));
        */
        if (m_ArrowRainCoroutine != null)
        {
            StopCoroutine(m_ArrowRainCoroutine);
        }
    }

    #region Skill
    [TitleGroup("Skill")] public float skillRange;
    [TitleGroup("Skill")] public GameObject skillPerfab;
    
    private Coroutine m_ArrowRainCoroutine;
    private GameObject m_SkillArrow;
    private GameObject m_SkillPerfab;
    
    protected override IEnumerator OnSkill()
    {
        float distance = 0f;
        GameObject text;
        AiController ai;
        int damage;
        bool isCrit;
        var txt = CharacterManager.instance.player.hurtText;
        for (int i = 0; i < CharacterManager.instance.enemyList.Count; i++)
        {
            distance = Vector3.Distance(m_SkillPerfab.transform.position, CharacterManager.instance.enemyList[i].transform.position);
            if (distance <= skillRange)
            {
                ai = CharacterManager.instance.enemyList[i];
                damage = GetSkillDamage(ai,out isCrit);
                ai.Injured(damage);
                text = PoolManager.instance.GetCell(txt);
                text.GetComponent<TxtMessage>().SetDamage(damage, isCrit, ai.transform.position);
            }
        }
        yield return null;
    }

    private int GetSkillDamage(AiController enemy,out bool isCrit)
    {
        var bouns = damage;
        
        if (crownBonus > 0 && healthy.GetHpPercent() >= 1f)
        {
            bouns += Mathf.CeilToInt((damage * crownBonus) * 0.01f);
        }
        
        if (capeBonus > 0 && healthy.GetHpPercent() <= 0.3f)
        {
            bouns += Mathf.CeilToInt((damage * capeBonus) * 0.01f);
        }
        
        if (crowbarBonus > 0f && enemy.GetHpPercent() >= 0.8f)
        {
            bouns += Mathf.CeilToInt((damage * crowbarBonus) * 0.01f);
        }

        if (axeBonus > 0f && enemy.GetHpPercent() <= 0.3f)
        {
            bouns += Mathf.CeilToInt((damage * axeBonus) * 0.01f);
        }

        isCrit = Random.Range(0f, 1f) <= critRate;
        return isCrit ? Mathf.CeilToInt(bouns * critDamage * 0.01f) : bouns;
    }
    
    protected override void OnSkillStart()
    {
        m_SkillPerfab = PoolManager.instance.GetCell(skillPerfab);
        m_SkillPerfab.transform.position = transform.position + new Vector3(0,0.1f,0);
//        skillRangeObj.SetActive(true);
//        m_ArrowRainCoroutine = 
        StartCoroutine(CreateArrowRain());
    }

    protected override void OnSkillEnd()
    {
        PoolManager.instance.Recycle(m_SkillPerfab);
//        skillRangeObj.SetActive(false);
//        StopCoroutine(m_ArrowRainCoroutine);
//        m_ArrowRainCoroutine = null;
    }

    private IEnumerator CreateArrowRain()
    {
        for (float i = 0; i < 4f; i+=0.1f)
        {
            /*
            if (CharacterManager.instance.player != null)
            {
                m_ArrowRainCoroutine = StartCoroutine(CreateArrowRain());
            }
            */
            m_SkillArrow = PoolManager.instance.GetCell(arrow);
            m_SkillArrow.transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));
            m_SkillArrow.transform.position =
                m_SkillPerfab.transform.position + new Vector3(Random.Range(-2.2f, 2.2f), 15, Random.Range(-2.2f, 2.2f));
            m_SkillArrow.GetComponent<AttackCell>().Init(20f, 0, false, 0, 0, null, null, false);
            yield return new WaitForSeconds(0.1f);
        }
    }

    #endregion
}
