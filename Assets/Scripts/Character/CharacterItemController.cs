﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using UnityEngine;

public class CharacterItemController : MonoBehaviour
{
    private CharacterHealthyController m_Healthy;
    private CharacterInputController m_Input;
    private CharacterAttackController m_Attack;

    private int m_ExtraReward;
    
    private void Awake()
    {
        m_Healthy = GetComponent<CharacterHealthyController>();
        m_Input = GetComponent<CharacterInputController>();
        m_Attack = GetComponent<CharacterAttackController>();
    }

    public void ShowItem(string item,bool isExtra = false)
    {
        var i = Instantiate(AssetBundleManager.GetAsset<GameObject>(item));
        var str = i.GetComponent<ItemInfo>().GetDescription(RewardManager.GetItemNumber(item) + 1);
        MessageManager.instance.ShowPanelWithOption(str, b =>
        {
            if (b)
            {
                RewardItem(item, isExtra);
            }
            else
            {
                if (!isExtra)
                {
                    GetExtraReward(item);
                }
            }
            Destroy(i);
        });
    }

    public void RewardItem(string item, bool isExtra = false)
    {
        var count = RewardManager.GetItemNumber(item);

        #region Normal

        if (CompareString(item, "Dagger"))
        {
            //Max 50%
            if (count >= 0 && count < 9)
            {
                m_Attack.IncreaseDamage(count == 0 ? 0.1f : 0.05f);
            }
        }
        else if (CompareString(item, "WoodenShield"))
        {
            //Max 10%
            if (count >= 0 && count < 8)
            {
                m_Healthy.IncreaseDefense(count == 0 ? 3 : 1);
            }
        }
        else if (CompareString(item, "ClothShoes"))
        {
            //Max 50%
            if (count >= 0 && count < 9)
            {
                m_Input.IncreaseMoveSpeed(count == 0 ? 0.1f : 0.05f);
            }
        }
        else if (CompareString(item, "LeatherArmor"))
        {
            //Max 50%
            if (count >= 0 && count < 9)
            {
                m_Healthy.IncreaseMaxHp(count == 0 ? 0.1f : 0.05f);
            }
        }
        else if (CompareString(item, "RoastWing"))
        {
            m_Healthy.Recover(0.05f);
        }

        #endregion

        #region Rare

        else if (CompareString(item, "BarrierCore"))
        {
            //Max 300
            if (count == 0)
            {
                m_Healthy.InitArmor(100);
            }
            else if (count >= 1 && count <= 9)
            {
                m_Healthy.IncreaseMaxArmor(20);
            }
        }
        else if (CompareString(item, "FrostGloves"))
        {
            //Max 30
            if (count == 0)
            {
                m_Attack.IncreaseSpeedCut(15);
            }
            else if (count >= 1 && count <= 5)
            {
                m_Attack.IncreaseSpeedCut(3);
            }
        }
        else if (CompareString(item, "ChillingPendant"))
        {
            //Max 10
            if (count == 0)
            {
                m_Attack.InitInRangeSpeedCut(5,3f);    
            }
            else if (count >= 1 && count <=5)
            {
                m_Attack.IncreaseInRangeSpeedCut(1);
            }
        }
        else if (CompareString(item, "DraculasFang"))
        {
            //Max 5
            if (count >= 0 && count < 5)
            {
                m_Attack.IncreaseHitEnemyRecoverHp(1);
            }
        }
        else if (CompareString(item, "SatansHeart"))
        {
            //Max 25
            if (count == 0)
            {
                m_Attack.IncreaseKillRecoverHp(10);
            }
            else if (count > 0 && count < 5)
            {
                m_Attack.IncreaseKillRecoverHp(3);
            }
        }
        else if (CompareString(item, "SlotMachine"))
        {
            //Max 45%
            if (count >= 0 && count < 5)
            {
                m_Attack.IncreaseCrit(count == 0 ? 0.1f : 0.05f,0);
            }
        }
        else if (CompareString(item, "Amplifier"))
        {
            //Max 125%
            if (count >= 0 && count < 11)
            {
                m_Attack.IncreaseCrit(0,count == 0 ? 25 : 10);
            }
        }
        else if (CompareString(item, "Scythe"))
        {
            //Max 8
            if (count == 0)
            {
                m_Attack.IncreaseCrit(0.05f,0);
                m_Attack.IncreaseCritEnemyRecoverHp(3);
            }
            else if (count >= 1 && count < 6)
            {
                m_Attack.IncreaseCritEnemyRecoverHp(1);
            }
        }
        else if (CompareString(item, "HyperGauntlet"))
        {
            m_Attack.SetAttackCount(true, 7 - count <= 5 ? 5 : 7 - count);
        }
        else if (CompareString(item, "Crowbar"))
        {
            //Max 120
            if (count >= 0 && count <= 7)
            {
                m_Attack.AddCrowbarBonus(count == 0 ? 50 : 10);
            }
        }
        else if (CompareString(item, "DualedgeAxe"))
        {
            //Max 60
            if (count >= 0 && count <= 3)
            {
                m_Attack.AddAxeBonus(count == 0 ? 30 : 10);
            }
        }
        else if (CompareString(item, "AcceleratingStopwatch"))
        {
            //Max 75
            if (count == 0)
            {
                //Clear RewindingStopwatch
                var rewindingSwCount = RewardManager.GetItemNumber("RewindingStopwatch");
                if (rewindingSwCount > 0)
                {
                    m_Attack.IncreaseAttackSpeed(0.2f);
                    m_Attack.IncreaseDamage((rewindingSwCount - 1) * -0.05f - 0.25f);
                }
                ClearItem("RewindingStopwatch");
                //Init AcceleratingStopwatch
                m_Attack.IncreaseAttackSpeed(0.25f);
                m_Attack.IncreaseDamage(-0.2f);
            }
            else if (count >= 1 && count <= 5)
            {
                m_Attack.IncreaseAttackSpeed(0.1f);
            }
        }
        else if (CompareString(item, "RewindingStopwatch"))
        {
            //Max 50
            if (count == 0)
            {
                //Clear AcceleratingStopwatch
                var acceleratingSwCount = RewardManager.GetItemNumber("AcceleratingStopwatch");
                if (acceleratingSwCount > 0)
                {
                    m_Attack.IncreaseDamage(0.2f);
                    m_Attack.IncreaseAttackSpeed((acceleratingSwCount - 1) * -0.1f - 0.25f);
                }
                ClearItem("AcceleratingStopwatch");
                //Init RewindingStopwatch
                m_Attack.IncreaseAttackSpeed(-0.2f);
                m_Attack.IncreaseDamage(0.25f);
            }
            else if (count >= 1 && count <= 5)
            {
                m_Attack.IncreaseDamage(0.05f);
            }
        }
        else if(CompareString(item,"HaresFoot"))
        {
            //max 25
            if(count < 5) IncreaseExtraReward(5);
        }
        else if (CompareString(item, "CrownofPride"))
        {
            //Max 75
            if (count >= 0 && count <= 10)
            {
                m_Attack.AddCrownBonus(count == 0 ? 25 : 5);
            }
        }
        else if (CompareString(item, "ScaleCape"))
        {
            //Max 150
            if (count >= 0 && count <= 10)
            {
                m_Attack.AddCapeBonus(count == 0 ? 50 : 10);
            }
        }

        #endregion

        #region Excellent

        #endregion

        RewardManager.AddItem(item);

        UIManager.instance.SetItem(item);

        if (!isExtra) GetExtraReward(item);
    }

    private void GetExtraReward(string item)
    {
        if(m_ExtraReward <= 0 || string.IsNullOrEmpty(item)) return;

        var level = RewardManager.GetItemLevel(item);

        if (m_ExtraReward >= 25 && RandomUtils.IsInRange(5))
        {
            //Reward higher level
            level++;
            Debug.Log("Get Higher Extra " + level);
            var seed = AssetBundleManager.GetAsset<RandomSeedAsset>(string.Concat(level, "ItemSeed"));
            ShowItem(RandomUtils.RandomSeed(seed), true);
            return;
        }

        if (RandomUtils.IsInRange(m_ExtraReward))
        {
            Debug.Log("Get Extra " + level);
            //Reward again
            if (level == RewardManager.ItemLevel.None) return;
            var seed = AssetBundleManager.GetAsset<RandomSeedAsset>(string.Concat(level, "ItemSeed"));
            ShowItem(RandomUtils.RandomSeed(seed),true);
        }
    }
    
    private void ClearItem(string item)
    {
        RewardManager.ClearItem(item);
        UIManager.instance.ClearItem(item);
    }
    
    private bool CompareString(string strA,string strB)
    {
        return string.Compare(strA, strB, StringComparison.Ordinal) == 0;
    }

    public void IncreaseExtraReward(int increase)
    {
        m_ExtraReward += increase;
    }
    
}
