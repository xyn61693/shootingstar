﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHealthyController : MonoBehaviour
{
    public int hp;
    private int m_MaxHp;
    private int m_BasisMaxHp;
    
    //private bool m_HasArmor;
    public float armorDelay = 10f;
    public float armorSpeed = 0.2f;
    //current armor
    private int m_Armor;
    //current max armor
    private int m_MaxArmor;
    //basis max armor
    private int m_BasisMaxArmor;
    private Coroutine m_RecoverArmorCoroutine;
    private Coroutine m_ArmorDelayCoroutine;
    
    private bool m_Invincible;
    public float invincibleTime = 1f;
    private Coroutine m_InvincibleCoroutine;

    private int m_DefensePercent;
    
    public GameObject hurtText;
    
    private Slider m_SldHp;
    private Text m_TxtHp;
    private Slider m_SldArmor;
    private Text m_TxtArmor;
    
    private CharacterInputController m_InputController;
    private CharacterAttackController m_AttackController;
    private Animator m_Animator;

    public GameObject recoverEffect;
    
    public Renderer[] renderers;
    
    private void Awake()
    {
        m_InputController = GetComponent<CharacterInputController>();
        m_AttackController = GetComponent<CharacterAttackController>();
        m_Animator = GetComponent<Animator>();
        
        m_MaxHp = m_BasisMaxHp = hp;
        m_MaxArmor = m_BasisMaxArmor = m_Armor = -1;
        
        m_SldHp = transform.parent.Find("UI/SldHp").GetComponent<Slider>();
        m_SldHp.value = 1f;
        m_TxtHp = transform.parent.Find("UI/TxtHp").GetComponent<Text>();
        m_TxtHp.text = hp.ToString();

        m_SldArmor = transform.parent.Find("UI/SldArmor").GetComponent<Slider>();
        m_SldArmor.value = 0f;
        m_SldArmor.gameObject.SetActive(false);
        m_TxtArmor = transform.parent.Find("UI/TxtArmor").GetComponent<Text>();
        m_TxtArmor.text = string.Empty;
        m_TxtArmor.enabled = false;
    }

    #region Hp

    public float GetHpPercent()
    {
        return hp / (float)m_MaxHp;
    }
    
    public void Recover(int recover)
    {
        if (hp + recover > m_MaxHp) hp = m_MaxHp;
        else hp += recover;
        PlayRecoverEffect(recover);
        UpdateHpUi();
    }

    public void Recover(float percent)
    {
        Recover((int) (m_MaxHp * percent));
    }
    
    public void ReduceMaxHp(int percent)
    {
        m_MaxHp = m_MaxHp * Mathf.CeilToInt((100 - percent) *0.01f);
        if (hp >= m_MaxHp) hp = m_MaxHp;
        UpdateHpUi();
    }

    //直接减少Hp
    public void ReduceHp(float percent)
    {
        hp = (int) (hp * (1- percent));
        UpdateHpUi();
    }
    
    public void IncreaseMaxHp(float percent)
    {
        m_MaxHp += (int) (m_BasisMaxHp * percent);
        UpdateHpUi();
        Debug.Log("IncreaseMaxHp "+ m_MaxHp);
    }

    //受到怪兽地形的伤害（正常流程，计算护甲防御
    public void Injured(int damage)
    {
        if (m_Invincible) return;
        //计算防御
        damage -= Mathf.CeilToInt(damage * m_DefensePercent * 0.01f);
        if (m_Armor + hp <= damage)
        {
            Dead();
            return;
        }
        //计算护甲
        if (m_Armor >= damage)
        {
            m_Armor -= damage;
            UpdateArmorUi();
            StartArmorDelay();
        }
        else if (m_Armor < damage && m_Armor > 0)
        {
            hp -= (damage - m_Armor);
            UpdateHpUi();
            m_Armor = 0;
            UpdateArmorUi();
            StartArmorDelay();
        }
        else
        {
            hp -= damage;
            UpdateHpUi();
        }
        PlayHurtTextEffect(damage);
        StartInvincible(invincibleTime);
        WhiteEffect();
    }

    public void Dead()
    {
        m_SldHp.value = 0f;
        m_TxtHp.text = "0";

        if (m_InputController) m_InputController.Kill();
        
        if (m_AttackController) m_AttackController.Kill();
        
        m_Animator.Play("Dead");
//        Destroy(transform.parent.gameObject);
        CharacterManager.instance.player = null;
    }

    #endregion
    
    #region Invincible
    private void StartInvincible(float time)
    {
        if (m_InvincibleCoroutine != null)
        {
        }
        else
        {
            m_TxtHp.DOFade(0.5f, 0.2f);
            m_InvincibleCoroutine = StartCoroutine(InvincibleCoroutine(time));
        }
    }

    private IEnumerator InvincibleCoroutine(float time)
    {
        m_Invincible = true;
        yield return new WaitForSeconds(time);
        m_Invincible = false;
        m_TxtHp.DOFade(1f, 0.2f);
        m_InvincibleCoroutine = null;
    }
    #endregion
    
    #region Armor
    public void InitArmor(int armor)
    {
        //m_HasArmor = true;
        m_MaxArmor = m_Armor = armor;
        UpdateArmorUi();
    }
    
    //开始计算护甲恢复延时
    private void StartArmorDelay()
    {
        //if(!m_HasArmor) return;
        if(m_Armor < 0) return;
        //取消恢复护甲
        if (m_RecoverArmorCoroutine != null)
        {
            StopCoroutine(m_RecoverArmorCoroutine);
            m_RecoverArmorCoroutine = null;
        }
        //重新开始计算延时
        if (m_ArmorDelayCoroutine != null)
        {
            StopCoroutine(m_ArmorDelayCoroutine);
        }
        m_ArmorDelayCoroutine = StartCoroutine(ArmorDelay(armorDelay));
    }

    private IEnumerator ArmorDelay(float time)
    {
        yield return new WaitForSeconds(time);
        m_ArmorDelayCoroutine = null;
        m_RecoverArmorCoroutine = StartCoroutine(RecoverArmorCoroutine());
    }

    private IEnumerator RecoverArmorCoroutine()
    {
        m_Armor++;
        UpdateArmorUi();
        if (m_Armor == m_MaxArmor)
        {
            m_RecoverArmorCoroutine = null;
            yield break;
        }
        yield return new WaitForSeconds(armorSpeed);
        m_RecoverArmorCoroutine = StartCoroutine(RecoverArmorCoroutine());
    }

    //增加护甲上限，当护甲不等于0时同时恢复护甲
    public void IncreaseMaxArmor(int increase)
    {
        m_MaxArmor += increase;
        if (m_Armor != 0)
        {
            RecoverArmor(increase);
        }
        UpdateArmorUi();
    }
    
    public void IncreaseMaxArmor(float percent)
    {
        IncreaseMaxArmor((int) (m_BasisMaxArmor * percent));
    }

    //恢复护甲
    public void RecoverArmor(int recover)
    {
        //if(!m_HasArmor) return;
        if(m_Armor < 0) return;
        if (m_Armor + recover >= m_MaxArmor)
        {
            m_Armor = m_MaxArmor;
        }
        else
        {
            m_Armor += recover;
        }
    }
    #endregion

    #region Defense

    public void IncreaseDefense(int percent)
    {
        m_DefensePercent += percent;
        Debug.Log(m_DefensePercent);
    }

    #endregion
    
    private void UpdateHpUi()
    {
        m_SldHp.value = (float) hp / m_MaxHp;
        m_TxtHp.text = hp.ToString();
    }

    private void UpdateArmorUi()
    {
        //m_SldArmor.gameObject.SetActive(m_HasArmor);
        //m_TxtArmor.enabled = m_HasArmor;
        m_SldArmor.gameObject.SetActive(m_Armor >= 0);
        m_TxtArmor.enabled = m_Armor >= 0;
        if (m_Armor < 0) return;
        m_SldArmor.value = (float) m_Armor / m_MaxArmor;
        m_TxtArmor.text = m_Armor.ToString();
    }
    
    private void PlayHurtTextEffect(int damage)
    {
        PoolManager.instance.GetCell(hurtText).GetComponent<TxtMessage>()
            .SetDamage(damage, true, transform.position);
    }

    private void PlayRecoverEffect(int recover)
    {
        PoolManager.instance.GetCell(hurtText).GetComponent<TxtMessage>()
            .SetRecover(recover,transform.position);
        var cell = PoolManager.instance.GetCell(recoverEffect);
        cell.transform.position = transform.position;
        cell.transform.SetParent(transform);
    }

    private void WhiteEffect()
    {
        Material mat;
        Sequence s = DOTween.Sequence();
        for (int i = 0; i < renderers.Length; i++)
        {
            mat = renderers[i].material;
            s.Append(mat.DOColor(Color.white, "_FresnelColor", 0.2f))
                .AppendInterval(invincibleTime - 0.4f)
                .Append(mat.DOColor(Color.black, "_FresnelColor", 0.2f));
        }
    }
}
