﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Networking.NetworkSystem;

public class CharacterInputController : MonoBehaviour
{
    public enum MoveState
    {
        Start,
        OnMove,
        End
    }

    private MoveState m_State = MoveState.End;
    
    public MoveState GetState()
    {
        return m_State;
    }
    
    public event Action<MoveState> onMoveStateChange = delegate{}; 
    
    public float moveSpeed;
    public float autoAimDistance = 5f;

    public Transform FootStep;
    public GameObject footStepDust;
    private Coroutine m_FootStepCoroutine;
    
    private float m_BasisMoveSpeed;
    
    private CharacterAttackController m_AttackController;
    
    private AiController m_LastAimTarget;
    private Vector3 m_AimTargetPos;
    
    private Animator m_Animator;
    private Rigidbody m_Rigidbody;
    
    private float m_InputStrenth;
    private Vector3 m_Input;

    private Tweener m_Tweener;
    private void Awake()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Rigidbody.maxAngularVelocity = 0f;
        m_AttackController = GetComponent<CharacterAttackController>();

        m_BasisMoveSpeed = moveSpeed;
        
        m_Animator.Play("Idle");
    }
    
    private void Update()
    {
        var strength = JoyStick.instance.GetInputStrength();
        m_Input = JoyStick.instance.GetAxisInput();
        
        if (strength > 0.01f)
        {
            if (Math.Abs(m_InputStrenth) < 0.01f)
            {
                StartMove();
            }
            m_InputStrenth = strength;
            OnMoving();
        }
        else
        {
            if (Math.Abs(m_InputStrenth) > 0.01f)
            {
                EndMove();
                m_InputStrenth = 0f;
            }
            OnStoppingMove();
        }
        AutoAimTarget();
    }

    private void StartMove()
    {
        m_State = MoveState.Start;
        onMoveStateChange.Invoke(MoveState.Start);
        m_Animator.Play("Move");
        m_Animator.speed = 1f;
        m_Tweener.Kill();
        m_FootStepCoroutine = StartCoroutine(CreateDust());
    }

    private void OnMoving()
    {
        m_State = MoveState.OnMove;
        onMoveStateChange.Invoke(MoveState.OnMove);
        if (m_Input != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(m_Input);
        }
        m_Animator.SetFloat("Speed",m_InputStrenth);
    }

    private void EndMove()
    {
        m_State = MoveState.End;
        onMoveStateChange.Invoke(MoveState.End);
        if (m_LastAimTarget)
        {
            m_AimTargetPos = m_LastAimTarget.transform.position;
            //m_Tweener = transform.DOLookAt(new Vector3(m_AimTargetPos.x, transform.position.y, m_AimTargetPos.z), 0.1f);
        }

        if (BattleManager.instance.isLevelClear)
        {
            m_Animator.Play("Idle");
        }
        else
        {
            m_Animator.Play("Attack");
            m_Animator.speed = m_AttackController.attackSpeed;
        }

        m_Rigidbody.angularVelocity = Vector3.zero;
        StopCoroutine(m_FootStepCoroutine);
        m_FootStepCoroutine = null;
    }

    private void OnStoppingMove()
    {
        if (m_LastAimTarget)
        {
            m_AimTargetPos = m_LastAimTarget.transform.position;
            transform.LookAt(new Vector3(m_AimTargetPos.x, transform.position.y, m_AimTargetPos.z));
//            m_Tweener = transform.DOLookAt(new Vector3(m_AimTargetPos.x, transform.position.y, m_AimTargetPos.z), 0.1f);
        }

        if (BattleManager.instance.isLevelClear && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("Attack"))
        {
            m_Animator.Play("Idle");
        }
    }
    
    private void FixedUpdate()
    {
        m_Rigidbody.velocity = m_Input * moveSpeed /* (m_InputStrenth >= 0.6f ? 1f : 0.3f)*/;
    }

    private void AutoAimTarget()
    {
        var distance = float.MaxValue;
        var index = -1;
        var enemy = CharacterManager.instance.enemyList;
        for (var i = 0; i < enemy.Count; i++)
        {
            var temp = Vector3.Distance(transform.position, enemy[i].transform.position);
            if (!(temp < distance) || !(temp <= autoAimDistance)) continue;
            distance = temp;
            index = i;
        }

        if (index >= 0)
        {
            var target = enemy[index].GetComponent<AiController>();
            SetAimSelectSprite(target);
        }
        else
        {
            if(m_LastAimTarget) m_LastAimTarget.SetIsAim(false);
            m_LastAimTarget = null;
        }
    }

    private void SetAimSelectSprite(AiController target)
    {
        if (m_LastAimTarget != null)
        {
            if (m_LastAimTarget == target) return;
            target.SetIsAim(true);
            m_LastAimTarget.SetIsAim(false);
            m_LastAimTarget = target;
        }
        else
        {
            m_LastAimTarget = target;
            target.SetIsAim(true);
        }
    }
    
    public void Kill()
    {
        if (m_FootStepCoroutine != null)
        {
            StopCoroutine(m_FootStepCoroutine);
            m_FootStepCoroutine = null;
        }
        m_Tweener.Kill();
        enabled = false;
    }

    public void IncreaseMoveSpeed(float percent)
    {
        moveSpeed += (m_BasisMoveSpeed * percent);
    }
    
    private IEnumerator CreateDust()
    {
        PoolManager.instance.GetCell(footStepDust).transform.position = FootStep.position;
        yield return new WaitForSeconds(0.2f);
        m_FootStepCoroutine = StartCoroutine(CreateDust());
    }
}
