﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ShowFPS : MonoBehaviour {
	
	public float updateInterval = 0.5f;
	private float m_LastInterval;
	private int m_Frames = 0;
	private int m_Fps = 0;

	private Text m_TxtFps;

	private void Awake()
	{
		m_TxtFps = GetComponent<Text>();
	}

	void Start()
	{
//		//设置帧率
		Application.targetFrameRate = 60;
 
		m_LastInterval = Time.realtimeSinceStartup;
		m_Frames = 0;
	}

	void Update()
	{
		++m_Frames;
		float timeNow = Time.realtimeSinceStartup;
		if (timeNow >= m_LastInterval + updateInterval)
		{
			m_Fps = Mathf.FloorToInt(m_Frames / (timeNow - m_LastInterval));
			m_Frames = 0;
			m_LastInterval = timeNow;
		}

		if (m_TxtFps)
		{
			m_TxtFps.text = m_Fps.ToString();
		}
	}

//	void OnGUI()
//	{
//		GUI.Label(new Rect(200, 500, 100, 50), fps.ToString());
//	}
}

