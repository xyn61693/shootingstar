﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddItemDev : MonoBehaviour
{
    private Button m_Button;

    private void Awake()
    {
        m_Button = GetComponent<Button>();
        m_Button.onClick.AddListener(Add);
    }
    
    private void Add()
    {
        CharacterManager.instance.player.GetComponent<CharacterItemController>().RewardItem(gameObject.name);
    }
}
