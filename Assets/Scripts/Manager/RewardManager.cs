﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class RewardManager
{
    #region Coin
    
    private static int m_Coin;

    public static int coin
    {
        get { return m_Coin; }
    }

    public static int currentCoinAltarPrice = 4;

    private static List<Coin> m_CoinList = new List<Coin>();
    
    public static void CreateCoin(Vector3 pos,int count)
    {
        var obj = AssetBundleManager.GetAsset<GameObject>("CoinPerfab t:Gameobject");
        if(obj == null) return;
        for (int i = 0; i < count; i++)
        {
            var cell = PoolManager.instance.GetCell(obj);
            cell.transform.position = pos + new Vector3(0, 1f, 0);
            cell.GetComponent<Rigidbody>().mass = 1;
            cell.GetComponent<Rigidbody>().AddForce(Random.Range(-200,200),100,Random.Range(-200,200));
            m_CoinList.Add(cell.GetComponent<Coin>());
            cell.GetComponent<Coin>().Fly(Random.Range(1f,1.5f));
        }
    }

    public static void FlyCoin()
    {
        for (int i = 0; i < m_CoinList.Count; i++)
        {
            m_CoinList[i].Fly();
        }
    }
    
    public static void ObtainCoin(int i,Coin c)
    {
        m_Coin += i;
        m_CoinList.Remove(c);
        UIManager.instance.SetCoin(m_Coin);
    }

    public static bool ConsumerCoin()
    {
        if (m_Coin >= currentCoinAltarPrice)
        {
            m_Coin -= currentCoinAltarPrice;
            UIManager.instance.SetCoin(m_Coin);
            currentCoinAltarPrice += 4;
            return true;
        }
        return false;
    }
    
    #endregion
    
    #region Altar
    public enum AltarType
    {
        Free,
        Coin,
        Hp,
        Life,
        Challenge
    }
    #endregion

    #region Item
    public enum ItemLevel
    {
        None = 0,
        Normal = 1,
        Rare = 2,
        Excellent = 3
    }

    public enum NoramlItem
    {
        Dagger,
        WoodenShield,
        ClothShoes,
//        Wand,
//        Robe,
        LeatherArmor,
        RoastWing,
//        RoastDrumstick
    }
    
    public enum RareItem
    {
        BarrierCore,
        FrostGloves,
        ChillingPendant,
        DraculasFang,
        SatansHeart,
        SlotMachine,
        Amplifier,
        Scythe,
//        TeslaCoil,
//        AirBlaster,
        HyperGauntlet,
//        IronSword,
//        SteelShield,
//        LeatherBoots,
//        CrystalWand,
//        SaintsRobe,
//        Chainmail,
//        RoastChicken,
        Crowbar,
        DualedgeAxe,
        AcceleratingStopwatch,
        RewindingStopwatch,
        HaresFoot,
        CrownofPride,
        ScaleCape,
    }
    
    public enum ExcellentItem
    {
//        SoulstoneFlame,
//        SoulstoneFreeze,
//        SoulstoneThunder,
//        SoulstoneDevastation
    }

    public static ItemLevel GetItemLevel(string item)
    {
        if (string.IsNullOrEmpty(item)) return ItemLevel.None;
        
        var names = Enum.GetNames(typeof(RewardManager.NoramlItem));
        if (names.Contains(item))
        {
            return ItemLevel.Normal;
        }

        names = Enum.GetNames(typeof(RewardManager.RareItem));
        if (names.Contains(item))
        {
            return ItemLevel.Rare;
        }

        names = Enum.GetNames(typeof(RewardManager.ExcellentItem));
        if (names.Contains(item))
        {
            return ItemLevel.Excellent;
        }
        
        return ItemLevel.None;
    }
    
    
    
    private static readonly Dictionary<string,int> m_Items = new Dictionary<string, int>();

    public static void AddItem(string item)
    {
        var count = GetItemNumber(item);
        if (count == 0)
        {
            m_Items.Add(item, 1);
        }
        else
        {
            count++;
            m_Items[item] = count;
        }
    }

    public static void ClearItem(string item)
    {
        m_Items.Remove(item);
    }
    
    public static int GetItemNumber(string item)
    {
        return !m_Items.ContainsKey(item) ? 0 : m_Items[item];
    }
    
    #endregion
}
