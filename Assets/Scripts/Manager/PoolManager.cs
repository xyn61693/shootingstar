﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : Singleton<PoolManager>
{
    private Dictionary<string,Queue<GameObject>> m_Dict = new Dictionary<string, Queue<GameObject>>();

    private Dictionary<string, Transform> m_CellTransforms = new Dictionary<string, Transform>();
    
    private Transform m_Pool;

    public void Init()
    {
        var manager = new GameObject {name = "PoolManager"};
        m_Pool = manager.transform;
    }
    
    public GameObject GetCell(GameObject obj,bool active = true)
    {
        var n = obj.name;
        var q = GetQueue(n);
        if (q.Count == 0)
        {
            var cell = Object.Instantiate(obj, m_CellTransforms[n]);
            cell.name = n;
            return cell;
        }
        var o = q.Dequeue();
        o.SetActive(active);
        return o;
    }

    public GameObject GetCell(string name, bool active = true)
    {
        return GetCell(AssetBundleManager.GetAsset<GameObject>(name),active);
    }
    
    public void Recycle(GameObject obj,bool active = false)
    {
        var n = obj.name;
        if (!m_Dict.ContainsKey(n))
        {
            Debug.Log("Pool does`t contains " + n);
            return;
        }
        obj.transform.SetParent(m_CellTransforms[n]);
        obj.SetActive(active);
        m_Dict[n].Enqueue(obj);
    }
    
    private Queue<GameObject> GetQueue(string key)
    {
        if (!m_Dict.ContainsKey(key))
        {
            var manager = new GameObject {name = key,transform = {parent = m_Pool}};
            m_CellTransforms.Add(key,manager.transform);
            m_Dict.Add(key,new Queue<GameObject>());
        }
        return m_Dict[key];
    }
}
