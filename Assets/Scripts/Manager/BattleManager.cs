﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.Serialization;
using Sirenix.OdinInspector;
using UnityEngine;

public class BattleManager : SerializedMonoBehaviour
{
   private static BattleManager m_Instance;
	
   public static BattleManager instance
   {
      get { return m_Instance; }
   }

   public List<MapInfoAsset> levelList = new List<MapInfoAsset>();
   
   public Dictionary<MapType,RandomSeedAsset> altarSeed = new Dictionary<MapType, RandomSeedAsset>();
   
   private int m_Level;

   public bool isLevelClear;

   public float waveBonus;

   public float bossAddWaveBonus;
   
   private void Awake()
   {
      m_Instance = this;
      var c = CharacterManager.instance;
   }

   private void Start()
   {
      StartBattle();
   }

   private void StartBattle()
   {
      m_Level = 0;
      MapInfoManager.instance.InitMap(levelList[m_Level]);
      UIManager.instance.level.text = string.Concat(m_Level," ",levelList[m_Level].name);
   }
   
   public void LevelClear()
   {
      if (m_Level == 0)
      {
         MapInfoManager.instance.CreateAltar("Free");
      }
      else
      {
         var type = MapInfoManager.instance.GetCurrentMapAsset().type;
         var altar = RandomUtils.RandomSeed(altarSeed[type]);
         MapInfoManager.instance.CreateAltar(altar);
      }
      //清算金币
      //RewardManager.FlyCoin();
      MapInfoManager.instance.SetDoorEnable(false);
      isLevelClear = true;
      if (levelList[m_Level].type == MapType.Boss)
      {
         waveBonus += bossAddWaveBonus;
      }
   }
   
   public void LoadNextLevel()
   {
      m_Level++;
      if (m_Level >= levelList.Count)
      {
         Debug.Log("Win");
      }
      else
      {
         isLevelClear = false;
         MapInfoManager.instance.RecycleMap();
         MapInfoManager.instance.InitMap(levelList[m_Level]);
         UIManager.instance.level.text = string.Concat(m_Level," ",levelList[m_Level].name);
      }
   }

   public float GetWaveBonus()
   {
      return (m_Level-1) * waveBonus;
   }

   #region Pause

   public bool isPause;
   
   public void Pause()
   {
      if(isPause) return;
      isPause = true;
      Time.timeScale = 0f;
   }

   public void Remuse()
   {
      if(!isPause) return;
      isPause = false;
      Time.timeScale = 1f;
   }
   
   #endregion
   
   private void Update()
   {
      if (Input.GetKeyDown(KeyCode.F1))
      {
         Debug.Log("Reset Altar");
         var type = MapInfoManager.instance.GetCurrentMapAsset().type;
         var altar = RandomUtils.RandomSeed(altarSeed[type]);
         MapInfoManager.instance.ResetAltar(altar);
      }
      if (Input.GetKeyDown(KeyCode.F2))
      {
         RewardManager.CreateCoin(CharacterManager.instance.player.transform.position + new Vector3(0,0,5),1);
      }

      if (Input.GetKeyDown(KeyCode.F3))
      {
         CharacterManager.instance.player.GetComponent<CharacterItemController>().RewardItem("CrownofPride");
      }

      if (Input.GetKeyDown(KeyCode.F4))
      {
         CharacterManager.instance.player.GetComponent<CharacterItemController>().RewardItem("ScaleCape");
      }
      
//      if (Input.GetKeyDown(KeyCode.F4))
//      {
//         MessageManager.instance.ShowPanelWithOption("Test");
//      }

      if (Input.GetKeyDown(KeyCode.F5))
      {
         CharacterManager.instance.player.GetComponent<CharacterAttackController>().OnAttackTest();
      }
   }
}
