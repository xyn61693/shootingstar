﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class MapInfoManager : SerializedMonoBehaviour
{
    private static MapInfoManager m_Instance;
	
    public static MapInfoManager instance
    {
        get { return m_Instance; }
    }

    public Transform ground;
    
    public GameObject door;

    public GameObject nextLevel;
    
    public Dictionary<MapInfo, GameObject> m_MapPrefab = new Dictionary<MapInfo, GameObject>();

    public Dictionary<EnemyType,GameObject> m_EnemyPrefab = new Dictionary<EnemyType, GameObject>();

    public Dictionary<RewardManager.AltarType, GameObject> altarList = new Dictionary<RewardManager.AltarType, GameObject>();
    
    private List<GameObject> m_MapCell = new List<GameObject>();
    
    private List<GameObject> m_Wall = new List<GameObject>();

    private List<GameObject> m_Water = new List<GameObject>();

    private GameObject m_Altar;
    
    private MapInfoAsset m_CurrentMapInfoAsset;
    
    public MapInfoAsset GetCurrentMapAsset()
    {
        return m_CurrentMapInfoAsset;
    }
    
//    private const int m_PassLayer = 23;
//    private const int m_DispassLayer = 24;
    
    private void Awake()
    {
        m_Instance = this;
    }

    public void InitMap(MapInfoAsset asset)
    {
        m_CurrentMapInfoAsset = asset;
        
        m_MapCell.Clear();
        m_Wall.Clear();
        m_Water.Clear();
        
        SetDoorEnable(true);
        
        //MapSize
        var size = asset.mapSize;
        ground.localScale = new Vector3(size.x,size.y,1);
        //ground.GetComponent<MeshRenderer>().sharedMaterial.SetTextureScale("_MainTex",size);
        ground.localPosition = Vector3.zero + new Vector3(size.x % 2 > 0 ? 0.5f : 0, 1,size.y % 2 > 0 ? 0.5f : 0);
        
        //Player
        CharacterManager.instance.player.transform.position = asset.player;
        
        //Map
        Vector3 mapPos;
        MapInfo info;
        GameObject cell;
        var mapKey = asset.map.Keys.ToList();
        for (int i = 0; i < mapKey.Count; i++)
        {
            mapPos = mapKey[i];
            info = asset.map[mapPos];
            cell = PoolManager.instance.GetCell(m_MapPrefab[info]);
            cell.transform.localPosition = mapPos;
            m_MapCell.Add(cell);
            if (info == MapInfo.Wall)
            {
                m_Wall.Add(cell);
            }
            else if (info == MapInfo.Water)
            {
                m_Water.Add(cell);
            }
        }

        //Enemy
        if (asset.enemy.Count <= 0)
        {
            BattleManager.instance.LevelClear();
        }
        else
        {
            Vector3 enemyPos;
            EnemyType enemyType;
            var enemyKey = asset.enemy.Keys.ToList();
            for (int i = 0; i < enemyKey.Count; i++)
            {
                enemyPos = enemyKey[i];
                enemyType = asset.enemy[enemyPos];
                CharacterManager.instance.CreateEnemy(m_EnemyPrefab[enemyType], enemyPos);
            }
        }
    }

    public void CreateAltar(string altar)
    {
        if (string.IsNullOrEmpty(altar)) return;
        RewardManager.AltarType type;
        Enum.TryParse(altar,out type);
        m_Altar = PoolManager.instance.GetCell(altarList[type]);
        m_Altar.transform.position = m_CurrentMapInfoAsset.altar;
    }

    public void ResetAltar(string altar)
    {
        m_Altar.GetComponent<Altar>().Recycle();
        CreateAltar(altar);
    }
    
    public void RecycleMap()
    {
        for (int i = 0; i < m_MapCell.Count; i++)
        {
            PoolManager.instance.Recycle(m_MapCell[i]);
        }
        
        if (m_Altar != null)
        {
            m_Altar.GetComponent<Altar>().Recycle();
        }
    }
    
    public void SetDoorEnable(bool enable)
    {
        door.SetActive(enable);
        nextLevel.SetActive(!enable);
    }
    
//    public void WallPass(bool enable)
//    {
//        for (int i = 0; i < m_Wall.Count; i++)
//        {
//            var col = m_Wall[i].GetComponent<Collider>();
//            col.gameObject.layer = enable ? m_PassLayer : m_DispassLayer;
//        }
//    }
//    
//    public void WaterPass(bool enable)
//    {
//        for (int i = 0; i < m_Water.Count; i++)
//        {
//            var col = m_Water[i].GetComponent<Collider>();
//            col.gameObject.layer = enable ? m_PassLayer : m_DispassLayer;
//        }
//    }
}
