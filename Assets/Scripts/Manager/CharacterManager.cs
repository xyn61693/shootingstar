﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class CharacterManager : Singleton<CharacterManager>
{
    public CharacterHealthyController player;
    
    public List<AiController> enemyList = new List<AiController>();

    private void Init()
    {
        Debug.Log("CharacterManager Init");
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<CharacterHealthyController>();
        Debug.Log("plyaer "+ player );
    }
    
    public GameObject CreateEnemy(GameObject prefab,Vector3 pos)
    {
        var e = PoolManager.instance.GetCell(prefab);
        var controller = e.GetComponentInChildren<AiController>();
        controller.transform.position = pos;
        controller.Reset();
        controller.StartCoroutine(controller.StartUpdateBehvior(0.5f));
        enemyList.Add(controller);
        return e;
    }

    public void RemoveEnemy(AiController e)
    {
        PoolManager.instance.Recycle(e.transform.parent.gameObject);
        enemyList.Remove(e);
        if (enemyList.Count == 0)
        {
            BattleManager.instance.LevelClear();
        }
    }
}
