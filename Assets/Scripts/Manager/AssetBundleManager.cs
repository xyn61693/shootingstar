﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class AssetBundleManager : Singleton<AssetBundleManager>
{
    private static AssetBundle m_AssetBundle;
    
    public void Init()
    {
        m_AssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "asset"));
    }

    public static T GetAsset<T>(string name) where T : Object
    {
#if UNITY_EDITOR
        var guid = AssetDatabase.FindAssets(name);
        if (guid.Length == 0)
        {
            Debug.LogWarning("Cant Find "+ name);
            return null;
        }
        return AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(guid[0]));
#else
        return m_AssetBundle.LoadAsset<T>(name);
#endif
    }
}
