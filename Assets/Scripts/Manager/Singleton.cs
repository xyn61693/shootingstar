﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class Singleton
{
	public static void Init(object instance)
	{
		var members = instance.GetType()
			.GetMember("Init", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
		if (members.Length > 0)
		{
			var mb = members.First() as MethodBase;
			if (mb != null)
			{
				try
				{
					mb.Invoke(instance, null);
				}
				catch (Exception ex)
				{
					Debug.LogWarning(instance.GetType() + "====" + ex);
				}
			}
		}
	}
}

public class Singleton<T> where T : new()
{
	private static T m_Instance;

	public static T instance
	{
		get
		{
			if (m_Instance == null)
			{
				m_Instance = new T();
				Singleton.Init(m_Instance);
			}
			return m_Instance;
		}
	}
}
