﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Sirenix.OdinInspector;
using Unity.Collections;
using UnityEngine;

[RequireComponent(typeof(AiController))]
public class AiBehavior : MonoBehaviour
{
    [Sirenix.OdinInspector.ReadOnly]
    public float weight;

    public bool isKill = false;
    
    private void Start()
    {
        OnInit();
    }

    public virtual void UpdateWeight()
    {
        weight = 1f;
    }
    
    public IEnumerator Excute()
    {
        if(isKill) yield break;
        yield return OnExcute();
    }

    public void Pause()
    {
    }
    
    public void Kill()
    {
        isKill = true;
        OnKill();
        StopCoroutine(Excute());
    }

    protected virtual IEnumerator OnExcute()
    {
        yield return null;
    }

    protected virtual void OnInit() { }
    
    protected virtual void OnKill() { }
}