﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq.Expressions;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class AiMove : AiBehavior
{
    public enum MoveType
    {
        Forward,
        Aim,
        Rush,
        Jump
    }
    public MoveType moveType;

    private bool m_IsForward() { return moveType == MoveType.Forward; }

    private bool m_IsAim() { return moveType == MoveType.Aim;}
    
    private bool m_IsRush() { return moveType == MoveType.Rush; }
    
    private bool m_IsJump() { return moveType == MoveType.Jump; }

    private Vector3 m_Velocity;

    [ShowIf("m_IsForward")] public float moveSpeed;
    [ShowIf("m_IsForward")] public Vector2 moveTime;
    [ShowIf("m_IsForward")]public Vector2 forwardAngle;

    [ShowIf("m_IsAim")] public Vector2 aimTime;
    
    [ShowIf("m_IsRush")] public Vector2 rushTime;
    [ShowIf("m_IsRush")] public float rushInterval = 6f;
    [ShowIf("m_IsRush")] public float rushSpeed;

    [ShowIf("m_IsJump")] public float jumpInterval = 5f;
    
    private Rigidbody m_Rigidbody;

    private AiShooting m_AiShooting;

    private AiAvoid m_AiAvoid;
    
    private Vector3[] m_Direction = {Vector3.forward,Vector3.left,Vector3.back,Vector3.right};
    
    protected override void OnInit()
    {
        m_AiShooting = GetComponent<AiShooting>();
        m_AiAvoid = GetComponentInChildren<AiAvoid>();
        m_Rigidbody = GetComponent<Rigidbody>();
        
        m_Rigidbody.useGravity = false;
        m_Rigidbody.mass = 1;
    }

    protected override void OnKill()
    {
        m_Rigidbody.velocity = Vector3.zero;
        if (m_SpeedCutCoroutine != null)
        {
            StopCoroutine(m_SpeedCutCoroutine);
            PoolManager.instance.Recycle(m_SpeedCutEffect);
        }
        if(m_AiAvoid!= null) m_AiAvoid.Kill();
    }

    public override void UpdateWeight()
    {
        if (m_IsJump()) weight = 1.1f;
        else base.UpdateWeight();
    }

    protected override IEnumerator OnExcute()
    {
        if (moveType == MoveType.Forward)
        {
            transform.DORotate(new Vector3(0, Random.Range(forwardAngle.x, forwardAngle.y), 0),0.2f);
            yield return new WaitForSeconds(0.2f);
            m_Rigidbody.velocity = transform.forward * moveSpeed;
            yield return new WaitForSeconds(Random.Range(moveTime.x,moveTime.y));
            m_Rigidbody.velocity = Vector3.zero;
        }
        else if (moveType == MoveType.Aim)
        {
            var t = Random.Range(aimTime.x, aimTime.y);
            t -= 0.2f;
            LookAtPlayer(0.2f);
            yield return new WaitForSeconds(0.2f);
            for (float i = t; i >= 0; i-= Time.deltaTime)
            {
                LookAtPlayer();
                yield return null;
            }
        }
        else if (moveType == MoveType.Rush)
        {
            if (rushInterval > 0f)
            {
                LookAtPlayer(rushInterval);
                yield return new WaitForSeconds(rushInterval);
            }
            else LookAtPlayer();
            
            if (m_AiAvoid != null)
            {
                if (m_AiAvoid.IsOnAvoid())
                {
                    m_Velocity = ApplySpeedCutEffect(transform.forward * rushSpeed);
                    m_Rigidbody.velocity = AvoidVelocity(m_Velocity, m_AiAvoid.GetNormal());
                }
                else
                {
                    m_Velocity = ApplySpeedCutEffect(transform.forward * rushSpeed);
                    m_Rigidbody.velocity = m_Velocity;
                }
            }
            else
            {
                m_Velocity = ApplySpeedCutEffect(transform.forward * rushSpeed);
                m_Rigidbody.velocity = m_Velocity;
            }
            yield return new WaitForSeconds(Random.Range(rushTime.x, rushTime.y));
        }
        else if (moveType == MoveType.Jump)
        {
            yield return new WaitForSeconds(jumpInterval);
            DOTween.To(() => m_Rigidbody.position, p => m_Rigidbody.MovePosition(p),
                    m_Rigidbody.position + m_Direction[Random.Range(0, m_Direction.Length)] * 2, 0.5f)
                .OnComplete(() =>
                {
                    if (m_AiShooting) StartCoroutine(m_AiShooting.Excute());
                });
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        if (m_IsAim()) return;
        if (m_AiAvoid != null)
        {
            if(!other.gameObject.CompareTag("Wall") || m_AiAvoid.IsOnAvoid()) return;
            //Debug.Log("Avoid "+ other.gameObject.tag);
            var normal = other.GetContact(0).normal;
            LookAtPlayer();
            //m_Velocity = transform.forward * rushSpeed;
            m_Rigidbody.velocity = AvoidVelocity(m_Velocity,normal);
            //Debug.Log("org v "+m_Velocity +" fix v "+ m_Rigidbody.velocity +" n "+ normal);
            m_AiAvoid.SetObstacle(other.gameObject,normal,() =>
            {
                //Debug.Log("Exit "+ m_Velocity);
                //m_Rigidbody.velocity = m_Velocity;
                /*
                if (CharacterManager.instance.player == null) return;
                LookAtPlayer();
                m_Rigidbody.velocity = transform.forward * rushSpeed;
                */
            });
        }
        else
        {
            m_Rigidbody.velocity = Vector3.zero;
        }
    }

    private Vector3 AvoidVelocity(Vector3 velocity,Vector3 normal)
    {
        var fix = velocity;
        if (Math.Abs(normal.x) < 0.01f && Math.Abs(normal.z) < 0.01f)
        {
            return fix;
        }
        if (Mathf.Abs(normal.z) > Mathf.Abs(normal.x))
        {
            if(IsOpposite(velocity.z,normal.z))  fix.z = 0f;
        }
        else
        {
            if(IsOpposite(velocity.x,normal.x))  fix.x = 0f;
        }
        return fix;
    }

    #region SpeedCut
    private int m_SpeedCut;
    private bool m_IsSpeedCuting;
    private Coroutine m_SpeedCutCoroutine;
    private int m_CoroutineSpeedCut;
    private GameObject m_SpeedCutEffect;
    private bool m_IsHasSpeedCutEffect;

    private Vector3 ApplySpeedCutEffect(Vector3 velocity)
    {
        if (m_SpeedCut <= 0) return velocity;
        var v = new Vector3(velocity.x,0,velocity.z);
        Debug.Log("Org "+velocity +" apply "+ (velocity - v * (m_SpeedCut * 0.01f)));
        return velocity - v * (m_SpeedCut * 0.01f);
    }

    private Vector3 ReverseSpeedCutEffect(Vector3 velocity,int speedcut)
    {
        if (speedcut <= 0) return velocity;
        var v = new Vector3(velocity.x,0,velocity.z);
        return velocity + v * (speedcut * 0.01f);
    }
    
    public void SetSpeedCut(int speedCut)
    {
        if(m_IsSpeedCuting) return;
        m_IsSpeedCuting = true;
        //Max 50 SpeedCut
        if (m_SpeedCut + speedCut >= 50)
        {
            speedCut -= (m_SpeedCut + speedCut) - 50;
        }
        m_SpeedCut += speedCut;
        m_Rigidbody.velocity = ApplySpeedCutEffect(m_Rigidbody.velocity);

        CreateSpeedCutEffect();
    }

    public void EndSpeedCut(int speedCut)
    {
        if(!m_IsSpeedCuting) return;
        m_IsSpeedCuting = false;
        
        if (m_SpeedCut >= 50) m_SpeedCut = 50;
        else if (m_SpeedCut <= 0) m_SpeedCut = 0;
        
        m_Rigidbody.velocity = ReverseSpeedCutEffect(m_Rigidbody.velocity, m_SpeedCut);
        if (m_SpeedCut - speedCut >= 0) m_SpeedCut -= speedCut;
        else m_SpeedCut = 0;

        RecycleSpeedCutEffect();
    }
    
    public void SetSpeedCut(int speedCut,float time)
    {
        if (m_SpeedCutCoroutine != null)
        {
            m_Rigidbody.velocity = ReverseSpeedCutEffect(m_Rigidbody.velocity, m_CoroutineSpeedCut);
            m_SpeedCut -= m_CoroutineSpeedCut;
            StopCoroutine(m_SpeedCutCoroutine);
            PoolManager.instance.Recycle(m_SpeedCutEffect);
        }
        m_SpeedCutCoroutine = StartCoroutine(SpeedCut(speedCut,time));
    }
    
    private IEnumerator SpeedCut(int speedCut,float time)
    {
        //Max 50 SpeedCut 
        if (m_SpeedCut + speedCut >= 50)
        {
            speedCut -= (m_SpeedCut + speedCut) - 50;
        }
        
        m_CoroutineSpeedCut = speedCut;
        m_SpeedCut += speedCut;
        m_Rigidbody.velocity = ApplySpeedCutEffect(m_Rigidbody.velocity);

        CreateSpeedCutEffect();
        
        yield return new WaitForSeconds(time);
        
        m_Rigidbody.velocity = ReverseSpeedCutEffect(m_Rigidbody.velocity, speedCut);
        m_SpeedCut -= speedCut;
        if (m_SpeedCut - speedCut >= 0) m_SpeedCut -= speedCut;
        else m_SpeedCut = 0;
        m_SpeedCutCoroutine = null;

        RecycleSpeedCutEffect();
    }

    private void CreateSpeedCutEffect()
    {
        if(m_IsHasSpeedCutEffect) return;
        m_IsHasSpeedCutEffect = true;
        m_SpeedCutEffect = PoolManager.instance.GetCell("Vfx_SpeedCut");
        m_SpeedCutEffect.transform.SetParent(transform);
        m_SpeedCutEffect.transform.position = transform.position;
    }

    private void RecycleSpeedCutEffect()
    {
        if(!m_IsHasSpeedCutEffect) return;
        m_IsHasSpeedCutEffect = false;
        PoolManager.instance.Recycle(m_SpeedCutEffect);
    }
    
    #endregion
    
    private bool IsOpposite(float a,float b)
    {
        return (a > 0 && b < 0) || (a < 0 && b > 0);
    }

    private void LookAtPlayer()
    {
        if(CharacterManager.instance.player == null) return;
        var target = CharacterManager.instance.player.transform.position;
        transform.LookAt(new Vector3(target.x, transform.position.y, target.z));
    }
    
    private void LookAtPlayer(float duration )
    {
        if(CharacterManager.instance.player == null) return;
        var target = CharacterManager.instance.player.transform.position;
        transform.DOLookAt(new Vector3(target.x, transform.position.y, target.z), duration);
    }

    /*
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            SetSpeedCut(50, 4f);
        }

        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            SetSpeedCut(30);
        }

        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            EndSpeedCut(30);
        }
    }
    */
}
