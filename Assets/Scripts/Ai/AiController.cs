﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class AiController : SerializedMonoBehaviour
{
    public int hp;
    private int m_BasisHp;

    public int attack;
    private int m_BasisAttack;
    
    public int coin;

    public RandomSeedAsset fallingItemSeed;

    public Dictionary<RewardManager.ItemLevel,RandomSeedAsset> item = new Dictionary<RewardManager.ItemLevel, RandomSeedAsset>();
    
    public SpriteRenderer aim;

    private bool m_IsDead = false;
    
    public enum ExcuteType
    {
        Random,
        Sequence
    }

    private bool m_IsSequence()
    {
        return excuteType == ExcuteType.Sequence;
    }
    
    public ExcuteType excuteType;
    
    private int m_SequenceIndex;
    
    private Text m_TxtHp;
    private Slider m_SldHp;
    private int m_MaxHp;
    
    [ShowIf("m_IsSequence"),ListDrawerSettings(ShowIndexLabels = true)]
    public List<AiBehavior> behaviors = new List<AiBehavior>();

    private void Awake()
    {
        if (!m_IsSequence())
        {
            behaviors = GetComponents<AiBehavior>().ToList();
        }

        m_BasisHp = hp;
        hp += Mathf.CeilToInt(hp * BattleManager.instance.GetWaveBonus());
        m_MaxHp = hp;
        m_SldHp = transform.parent.Find("UI/SldHp").GetComponent<Slider>();
        m_SldHp.value = 1f;
        
        m_TxtHp = transform.parent.Find("UI/TxtHp").GetComponent<Text>();
        m_TxtHp.text = hp.ToString();

        m_BasisAttack = attack;
        attack += Mathf.CeilToInt(attack * BattleManager.instance.GetWaveBonus());
    }

    public IEnumerator StartUpdateBehvior(float time = 0f)
    {
        if (time != 0f)
        {
            yield return new WaitForSeconds(time);
        }

        if(m_IsSequence())
        {
            StartCoroutine(ExcuteBehavior(m_SequenceIndex));
            m_SequenceIndex++;
            if (m_SequenceIndex >= behaviors.Count) m_SequenceIndex = 0;
        }
        else
        {
            UpdateBehavior();
            StartCoroutine(ExcuteBehavior(0));
        }
    }

    public void SetIsAim(bool enable)
    {
        aim.gameObject.SetActive(enable);
    }

    private void UpdateBehavior()
    {
        for (var i = 0; i < behaviors.Count; i++)
        {
            behaviors[i].UpdateWeight();
        }
        behaviors.Sort((a, b) => b.weight.CompareTo(a.weight));
    }

    private IEnumerator ExcuteBehavior(int behaviorIndex)
    {
        yield return behaviors[behaviorIndex].Excute();
        if(CharacterManager.instance.player) StartCoroutine(StartUpdateBehvior());
    }
    
    public bool Injured(int damage)
    {
        var isDead = hp <= damage;
        if (isDead) Dead();
        else
        {
            hp -= damage;
            m_SldHp.value = (float)hp/m_MaxHp;
            m_TxtHp.text = hp.ToString();
        }
        return isDead;
    }

    //剩余血量百分比
    public float GetHpPercent()
    {
        return hp / (float)m_MaxHp;
    }

    private void Dead()
    {
        m_IsDead = true;
        for (int i = 0; i < behaviors.Count; i++)
        {
            behaviors[i].Kill();
        }
        RewardManager.CreateCoin(transform.position,coin);
        FallingItem();
        CharacterManager.instance.RemoveEnemy(this);
        PlayDeathEffect();
    }
    
    private void PlayDeathEffect()
    {
        CameraShake.instance.ShakeCamera();
        var cell = PoolManager.instance.GetCell("Vfx_Death");
        cell.transform.position = transform.position;
    }
    
    public void Reset()
    {
        if(!m_IsDead) return;
        for (int i = 0; i < behaviors.Count; i++)
        {
            behaviors[i].isKill = false;
        }
        
        hp = m_MaxHp = m_BasisHp + Mathf.CeilToInt(m_BasisHp * BattleManager.instance.GetWaveBonus());
        m_SldHp.value = (float)hp/m_MaxHp;
        m_TxtHp.text = hp.ToString();
        
        attack = m_BasisAttack + Mathf.CeilToInt(m_BasisAttack * BattleManager.instance.GetWaveBonus());
        
        m_IsDead = false;
        if (m_IsSequence()) m_SequenceIndex = 0;
    }

    private void FallingItem()
    {
        var l = RandomUtils.RandomSeed(fallingItemSeed);
        if (l == "None") return;
        RewardManager.ItemLevel level;
        Enum.TryParse(l, out level);
        var i = RandomUtils.RandomSeed(item[level]);
        CharacterManager.instance.player.GetComponent<CharacterItemController>().ShowItem(i);
    }
}
