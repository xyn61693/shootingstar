﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Sirenix.OdinInspector;
using UnityEngine;

public class AiShooting : AiBehavior
{
    public GameObject cellPrefab;
    public Transform shootPos;

    public enum ShootingType
    {
        Forward,
        Angle,
        Random,
//        Custom
    }

    public ShootingType type;

    private bool m_IsShowCellInterval()
    { return cellCount > 1 && (type == ShootingType.Forward || type == ShootingType.Random); }

    private bool m_IsAngle() { return type == ShootingType.Angle; }
    
//    private bool m_IsCustom() { return type == ShootingType.Custom; }
    
    //public int damage;
    
    public float cellSpeed;
    
    public float attackInterval;
    
    [MinValue(1)]
    public int cellCount;
    
    [ShowIf("m_IsShowCellInterval")]
    public float cellInterval = 0.2f;
    
    [ShowIf("m_IsAngle")]
    public float attackAngle;

    private List<Transform> m_ShootPosArray = new List<Transform>();

    private AiController m_Controller;
    
    protected override void OnInit()
    {
        m_Controller = GetComponentInParent<AiController>();
        
        var child = shootPos.childCount;
        for (int i = 0; i < child; i++)
        {
            m_ShootPosArray.Add(shootPos.GetChild(i));
        }
    }

    protected override IEnumerator OnExcute()
    {
        yield return new WaitForSeconds(attackInterval);
        yield return OnShooting();
    }

    private IEnumerator OnShooting()
    {
        if (type == ShootingType.Forward)
        {
            for (int i = 0; i < cellCount; i++)
            {
                CreateCell(0f);
                yield return new WaitForSeconds(cellInterval);
            }
        }
        else if (type == ShootingType.Angle)
        {
            var interval = attackAngle / cellCount;
            for (int i = 0; i < cellCount; i++)
            {
                var offset = interval * (i - cellCount / 2);
                CreateCell(offset);
            }
        }
        else if(type == ShootingType.Random)
        {
            shootPos.Rotate(new Vector3(0,Random.Range(0f,360f)));
            for (int i = 0; i < cellCount; i++)
            {
                CreateCell(0f);
                yield return new WaitForSeconds(cellInterval);
            }
        }
//        else if(type == ShootingType.Custom)
//        {
//            yield return null;
//        }
    }

    private void CreateCell(float angle)
    {
        for (int i = 0; i < m_ShootPosArray.Count; i++)
        {
            var cell = PoolManager.instance.GetCell(cellPrefab,false);
            var euler = m_ShootPosArray[i].eulerAngles;
            var ro = new Vector3(euler.x, euler.y + angle, euler.z);
            cell.transform.SetPositionAndRotation(m_ShootPosArray[i].position, Quaternion.Euler(ro));
            cell.gameObject.SetActive(true);
            cell.GetComponent<AttackCell>().Init(cellSpeed, m_Controller.attack,false,0,0,null,null);
        }
    }
    
    protected override void OnKill()
    {
        StopCoroutine(OnShooting());
    }
}
