﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiCreate : AiBehavior
{
    public List<GameObject> create;

    [Range(0,100)]
    public int createChance;

    protected override IEnumerator OnExcute()
    {
        if (RandomUtils.IsInRange(createChance))
        {
            for (var i = 0; i < create.Count; i++)
            {
                CharacterManager.instance.CreateEnemy(create[i], transform.position);
            }
        }
        yield return null;
    }
}
