﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class AiAvoid : MonoBehaviour
{
    private GameObject m_Obstacle;
    
    private Vector3 m_Normal;
    
    private Action m_AfterAvoid;

    private bool m_IsOnAvoid;
    
    private void Awake()
    {
        GetComponent<Collider>().isTrigger = true;
    }

    public Vector3 GetNormal()
    {
        return m_Normal;
    }

    public bool IsOnAvoid()
    {
        return m_IsOnAvoid;
    }

    public void SetObstacle(GameObject obstacle,Vector3 normal,Action cb = null)
    {
        m_Obstacle = obstacle;
        m_Normal = normal;
        m_AfterAvoid = cb;
        m_IsOnAvoid = true;
    }

    public void Kill()
    {
        m_Obstacle = null;
        m_AfterAvoid = null;
        m_IsOnAvoid = false;
    }

    private void OnTriggerExit(Collider other)
    {
        if(!other.gameObject.CompareTag("Wall") || other.gameObject != m_Obstacle) return;
        m_Obstacle = null;
        m_IsOnAvoid = false;
        if (m_AfterAvoid != null)
        {
            m_AfterAvoid.Invoke();
            m_AfterAvoid = null;
        }
    }
}
