﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class AiAttack : MonoBehaviour
{
    private bool m_IsAttack;

    private AiController m_Controller;
    
    private void Awake()
    {
        m_Controller = GetComponentInParent<AiController>();
    }
    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player") && !m_IsAttack)
        {
            m_IsAttack = true;
            other.GetComponent<CharacterHealthyController>().Injured(m_Controller.attack);
        }
    }
    
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            m_IsAttack = false;
        }
    }
    

}
