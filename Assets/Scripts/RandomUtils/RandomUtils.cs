﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RandomUtils
{
    private static float m_RandomResult;
    private static int m_CurrentWeight;
    
    public static string RandomSeed(RandomSeedAsset asset)
    {
        if (asset == null) return null;
        m_RandomResult = Random.Range(0f, asset.maxSeedRange);
        var keyList = asset.seed.Keys.ToList();
        for (int i = 0; i < keyList.Count; i++)
        {
            if (IsContain(m_RandomResult, asset.seed[keyList[i]]))
            {
                return keyList[i];
            }
        }
        return null;
    }

    public static string RandomSeed(Dictionary<string,int> weight,int max)
    {
        m_CurrentWeight = 0;
        m_RandomResult = Random.Range(0f, max);
        var keyList = weight.Keys.ToList();
        Vector2 range;
        for (int i = 0; i < keyList.Count; i++)
        {
            range = new Vector2(m_CurrentWeight, weight[keyList[i]] + m_CurrentWeight);
            if (IsContain(m_RandomResult, range))
            {
                return keyList[i];
            }
            m_CurrentWeight = weight[keyList[i]];
        }
        return null;
    }
    
    private static bool IsContain(float f,Vector2 range)
    {
        return f > range.x && f < range.y;
    }
    
    public static bool IsInRange(float rate,int min = 0, int max = 100)
    {
        m_RandomResult = Random.Range(min, max);
        return m_RandomResult < rate;
    }
}
