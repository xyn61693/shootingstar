﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName ="SeedAsset.asset",menuName = "SeedAsset")]
public class RandomSeedAsset : SerializedScriptableObject
{
    public enum SeedType
    {
        Altar,
        ItemLevel,
        NormalItem,
        RareItem,
        ExcellentItem,
    }

    [OnValueChanged("OnSeedTypeChange")]public SeedType type;

    private bool m_IsItemType;
    
    private void OnSeedTypeChange()
    {
        infos.Clear();
        Type t = null;
        
        if (type == SeedType.Altar)
            t = typeof(RewardManager.AltarType);
        else if (type == SeedType.ItemLevel)
            t = typeof(RewardManager.ItemLevel);
        else if (type == SeedType.NormalItem)
            t = typeof(RewardManager.NoramlItem);
        else if (type == SeedType.RareItem)
            t = typeof(RewardManager.RareItem);
        else if (type == SeedType.ExcellentItem) 
            t = typeof(RewardManager.ExcellentItem);

        if (t == null) return;
        var nameList = Enum.GetNames(t);
        for (int i = 0; i < nameList.Length; i++)
        {
            infos.Add(new SeedInfo(){name = nameList[i],weight = 1});
        }
    }
    
    public struct SeedInfo
    {
        [ReadOnly,HorizontalGroup("Seed"),HideLabel] public string name;
        [HorizontalGroup("Seed")]public int weight;
    }

    public List<SeedInfo> infos = new List<SeedInfo>();
    
    private SeedInfo m_Info;

    private Vector2 m_Range;

    [Button]
    public void AverageWeight()
    {
        for (int i = 0; i < infos.Count; i++)
        {
            m_Info = infos[i];
            m_Info.weight = 10;
            infos[i] = m_Info;
        }
    }
    
    [Button(ButtonSizes.Medium,Name = "Save")]
    public void SaveSeed()
    {
        seed.Clear();
        maxSeedRange = 0;
        m_Range = Vector2.zero;

        for (int i = 0; i < infos.Count; i++)
        {
            m_Info = infos[i];
            m_Range = new Vector2(maxSeedRange, m_Info.weight + maxSeedRange);
            maxSeedRange += m_Info.weight;
            seed.Add(m_Info.name, m_Range);
        }
    }

    [ReadOnly,PropertyOrder(1)] public int maxSeedRange;
    
    [ReadOnly,PropertyOrder(1)] public Dictionary<string,Vector2> seed = new Dictionary<string, Vector2>();

    public void ChangeSeedWeight(string name, int weight)
    {
        for (int i = 0; i < infos.Count; i++)
        {
            m_Info = infos[i];
            if (name == m_Info.name)
            {
                m_Info.weight = weight;
                infos[i] = m_Info;
                break;
            }
        }
    }
}
