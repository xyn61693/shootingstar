﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.Serialization;
using UnityEngine;

public class DestroyEffect : MonoBehaviour
{

    private ParticleSystem m_System;

    private void Awake()
    {
        m_System = GetComponent<ParticleSystem>();
        var main = m_System.main;
        main.stopAction = ParticleSystemStopAction.Callback;
    }

    public void Play()
    {
        m_System.Play(true);
    }

    private void OnDisable()
    {
        m_System.Stop(true);
    }

    private void OnParticleSystemStopped()
    {
        PoolManager.instance.Recycle(gameObject);
    }
}
