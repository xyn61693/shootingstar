﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;

public class MessageManager : MonoBehaviour
{
    private static MessageManager m_Instance;
	
    public static MessageManager instance
    {
        get { return m_Instance; }
    }
    
    private Text m_Text;
    private CanvasGroup m_Group;
    private Image m_Mask;
    private GameObject m_Opiton;

    private bool m_IsShowing;
    private bool m_IsWitchOption;
    private Action<bool> m_CbOption;
    private Action m_Cb;
    
    private Vector3 m_Pos;
    
    private void Awake()
    {
        m_Instance = this;
        m_Text = GetComponentInChildren<Text>();
        m_Mask = transform.Find("Mask").GetComponent<Image>();
        m_Group = GetComponent<CanvasGroup>();
        m_Opiton = transform.Find("Option").gameObject;
        m_Pos = transform.position;
        transform.position = m_Pos + new Vector3(0, 1000f);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (m_Opiton.activeInHierarchy)
            {
                Cancel();
            }
            else
            {
                Close();
            }
        }
        else if (Input.GetKeyDown(KeyCode.Space))
        {
            if (m_Opiton.activeInHierarchy)
            {
                Submit();
            }
            else
            {
                Close();
            }
        }
    }

    #region Panel
    public void ShowPanel(string content, Action cb = null)
    {
        if(m_IsShowing) return;
        OpenPanel(content);
        m_Cb = cb;
    }
    
    public void ShowPanelWithOption(string content,Action<bool> option = null)
    {
        if(m_IsShowing) return;
        OpenPanel(content);
        m_IsWitchOption = true;
        m_Opiton.SetActive(true);
        m_CbOption = option;
    }

    public void Close()
    {
        if(m_IsWitchOption) return;
        ClosePanel(m_Cb);
    }

    public void Cancel()
    {
        ClosePanel(false,m_CbOption);
    }

    public void Submit()
    {
        ClosePanel(true,m_CbOption);
    }
    
    private void OpenPanel(string content)
    {
        m_IsShowing = true;
        m_Mask.gameObject.SetActive(true);
        m_Text.text = content;
        transform.DOMove(m_Pos, 0.1f);
        m_Group.DOFade(1f, 0.1f)
            .OnComplete(() => { BattleManager.instance.Pause();});
        JoyStick.instance.SetJoyStickEnable(false);
        CharacterManager.instance.player.GetComponent<CharacterAttackController>().SetSkill(false);
    }

    private void ClosePanel(Action cb = null)
    {
        BattleManager.instance.Remuse();
        m_IsShowing = false;
        m_Mask.gameObject.SetActive(false);
        transform.DOMove(m_Pos + new Vector3(0, 1000f), 0.2f);
        m_Group.DOFade(0f, 0.2f).OnComplete(() =>
        {
            m_Text.text = string.Empty;
            if (cb != null) cb.Invoke();
        });
        JoyStick.instance.SetJoyStickEnable(true);
        CharacterManager.instance.player.GetComponent<CharacterAttackController>().SetSkill(true);
    }
    
    private void ClosePanel(bool b = false,Action<bool> cb = null)
    {
        BattleManager.instance.Remuse();
        m_IsShowing = false;
        m_Mask.gameObject.SetActive(false);
        transform.DOMove(m_Pos + new Vector3(0, 1000f), 0.2f);
        m_Group.DOFade(0f, 0.2f).OnComplete(() =>
        {
            m_Text.text = string.Empty;
            if (cb != null) cb.Invoke(b);
        });
        JoyStick.instance.SetJoyStickEnable(true);
        CharacterManager.instance.player.GetComponent<CharacterAttackController>().SetSkill(true);
        m_IsWitchOption = false;
        m_Opiton.SetActive(false);
    }
    #endregion
    
    #region Message
    
    #endregion
    
    
}
