﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemInfo : PointerHandler
{
    private Image m_Icon;
    private Text m_CountText;

    private int m_Count;
    
    [Multiline(3)]
    public string description;

    public int effectValue;

    public int initialBonus;

    public int maxCount;
    
    private void Awake()
    {
        m_Icon = GetComponent<Image>();
        m_CountText = transform.Find("Count").GetComponent<Text>();
    }

    public void AddCount()
    {
        m_Count ++;
        m_CountText.text = m_Count.ToString();
    }

    public string GetDescription()
    {
        return description.Replace("#Count#",
            string.Concat("<color=red>", (m_Count >= maxCount ? maxCount : m_Count) * effectValue + initialBonus, "</color>"));
    }

    public string GetDescription(int count)
    {
        return description.Replace("#Count#", string.Concat("<color=red>",count * effectValue + initialBonus,"</color>"));
    }
    
    protected override void OnMouseEnter()
    {
        UIManager.instance.SetItemDescription(true,GetDescription());
    }

    protected override void OnMouseExit()
    {
        UIManager.instance.SetItemDescription(false);
    }
}
