﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : SerializedMonoBehaviour
{
    private static UIManager m_Instance;
	
    public static UIManager instance
    {
        get { return m_Instance; }
    }
    
    public Text coin;

    public Transform itemIcon;
    
    public Text description;

    public Text level;
    
    private List<GameObject> m_Icons = new List<GameObject>();
    
    public void Awake()
    {
        m_Instance = this;
    }

    public void SetCoin(int c)
    {
        coin.text = c.ToString();
    }

    public void SetItem(string items)
    {
        //排除消耗性道具
        if(items == "RoastWing") return;
        
        var i = GetItem(items);
        if (i == null)
        {
            i = Instantiate(AssetBundleManager.GetAsset<GameObject>(items + " t:GameObject"));
            i.transform.SetParent(itemIcon);
            i.name = items;
            m_Icons.Add(i);
        }
        i.GetComponent<ItemInfo>().AddCount();
    }

    public void ClearItem(string items)
    {
        var i = GetItem(items);
        if (i != null)
        {
            m_Icons.Remove(i);
            Destroy(i);
        }
    }
    
    private GameObject GetItem(string item)
    {
        for (int i = 0; i < m_Icons.Count; i++)
        {
            if (m_Icons[i].name == item)
            {
                return m_Icons[i];
            }
        }
        return null;
    }

    public void SetItemDescription(bool active,string s = null)
    {
        description.gameObject.SetActive(active);
        if(active && !string.IsNullOrEmpty(s)) description.text = s;
    }

    public Transform Find(string child)
    {
        return transform.Find(child);
    }
}
