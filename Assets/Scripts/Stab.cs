﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stab : MonoBehaviour
{
    public int damage;

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Stab Enter");
        if (other.CompareTag("Player"))
        {
            other.gameObject.GetComponent<CharacterHealthyController>().Injured(damage);
        }
    }
}
