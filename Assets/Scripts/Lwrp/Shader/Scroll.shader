﻿Shader "Pero/Scroll"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color",color) = (1,1,1,1)
        
		_Mask("Mask",float) = 0.5

        _ScrollTex("Scroll Texture",2D) = "white" {}
        _Speed("Speed",float) = 1
        
        [Toggle(EMISSION)]_EmissionEnable("Emission",float) = 0
        [HDR]_Emission("Emission",color) = (0,0,0,0)
        _EmissionTex("Emission Texture",2D) = "black"{}
        
        [Toggle(FOG)]_FogEnable("Fog",float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" "RenderPipeline" = "LightweightPipeline"}
        //LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma shader_feature FOG
            #pragma shader_feature EMISSION
            #pragma multi_compile_fog

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                UNITY_FOG_COORDS(1)
            };

            CBUFFER_START(UnityPerMaterial)

            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _ScrollTex;
            float4 _ScrollTex_ST;

            fixed _Speed;

            fixed4 _Color;
            //fixed _FogEnable;

			fixed _Mask;
            
            fixed4 _Emission;
            sampler2D _EmissionTex;
            float4 _EmissionTex_ST;
            
            CBUFFER_END
            
            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            float2 ScrollUv(float2 uv)
            {
                uv.y += _Time.x * _Speed;
                return uv;
            }
            

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex,i.uv);
                fixed mask = step(_Mask,col);
                fixed4 scroll = tex2D(_ScrollTex,ScrollUv(i.uv)) * mask;
                
                #if FOG
                UNITY_APPLY_FOG(i.fogCoord, col);
                #endif
                
                fixed4 emission = tex2D(_EmissionTex,i.uv) * _Emission;
                
                return scroll + col * (1- mask);
                //return col * _LightColor0 * _Color + emission;
            }
            ENDCG
        }
    }
}
