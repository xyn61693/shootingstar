﻿Shader "Pero/Default"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color",color) = (1,1,1,1)
        
        [Toggle(EMISSION)]_EmissionEnable("Emission",float) = 0
        [HDR]_Emission("Emission",color) = (0,0,0,0)
        _EmissionTex("Emission Texture",2D) = "black"{}
        
        [Toggle(FOG)]_FogEnable("Fog",float) = 0
    }
    SubShader
    {
        Tags { "RenderPipeline"="LightweightPipeline" "RenderType"="Opaque" "Queue"="Geometry" }
		Cull Back
		HLSLINCLUDE
		#pragma target 2.0
		ENDHLSL
		
        Pass
        {
        	Tags {"LightMode"="LightweightForward"}
        	Name "Base"
			Blend One Zero
			ZWrite On
			ZTest LEqual
            
        	HLSLPROGRAM
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x

        	// -------------------------------------
            // Lightweight Pipeline keywords
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            //#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            //#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile _ _SHADOWS_SOFT
            #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE
            
        	// -------------------------------------
            // Unity defined keywords
            #pragma multi_compile _ DIRLIGHTMAP_COMBINED
            #pragma multi_compile _ LIGHTMAP_ON
            #pragma multi_compile_fog

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

        	#include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/Core.hlsl"
        	//#include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/Lighting.hlsl"

            #pragma vertex vert
        	#pragma fragment frag
            
            #pragma shader_feature EMISSION
            #pragma shader_feature FOG
            
			sampler2D _MainTex;
			sampler2D _EmissionTex;
			
			CBUFFER_START(UnityPerMaterial)
			half3 _Color;
			half4 _MainTex_ST;
            
            half4 _Emission;
            float4 _EmissionTex_ST;
            CBUFFER_END
            
            struct GraphVertexInput
            {
                float4 vertex       : POSITION;
                float3 normal       : NORMAL;
                float4 texcoord     : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

        	struct GraphVertexOutput
            {
                float4 clipPos                  : SV_POSITION;
        		half fogFactor                  : TEXCOORD1; // x: fogFactor
            	float3 normal                   : TEXCOORD3;
				float4 texcoord                 : TEXCOORD7;
				
                UNITY_VERTEX_INPUT_INSTANCE_ID
            	UNITY_VERTEX_OUTPUT_STEREO
            };

			half4 LightweightFragmentUnlit(half3 Albedo, half3 Emission, half Alpha,half fogFactor)
            {
                half3 finalColor = Albedo * _Color;
                
                #if EMISSION
                finalColor += Emission;
                #endif
                
                #if FOG
                finalColor.rgb = MixFog(finalColor.rgb, fogFactor.x);
                #endif
                
                return half4(finalColor,Alpha);
            }
			
            GraphVertexOutput vert (GraphVertexInput v )
        	{
        		GraphVertexOutput o = (GraphVertexOutput)0;
                UNITY_SETUP_INSTANCE_ID(v);
            	UNITY_TRANSFER_INSTANCE_ID(v, o);
        		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);
                
                o.texcoord.xy = v.texcoord.xy;
                o.texcoord.zw = 0;
                
                o.normal = TransformObjectToWorldNormal(v.normal);
                VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex.xyz);

        	    o.fogFactor = ComputeFogFactor(vertexInput.positionCS.z);
        	    
        	    o.clipPos = vertexInput.positionCS;
        		
        		return o;
        	}
            
        	half4 frag (GraphVertexOutput IN) : SV_Target
            {
            	UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN);
                
				float2 uvMainTex = IN.texcoord.xy * _MainTex_ST.xy + _MainTex_ST.zw;

                float3 Albedo = tex2D( _MainTex, uvMainTex ).rgb;
                float3 Emission = tex2D(_EmissionTex,uvMainTex).rgb * _Emission.rbg;
                
        		return LightweightFragmentUnlit(Albedo,Emission,1,IN.fogFactor);
            }
        	ENDHLSL
        }
    }
}
