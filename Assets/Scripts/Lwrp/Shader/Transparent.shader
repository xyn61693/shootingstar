﻿Shader "Pero/Transparent"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color("Color",color) = (1,1,1,1)
        
        _CutOff("CutOff",range(0,1)) = 0
        
        [Toggle(RIM)]_FresnelEnable("Rim",float) = 0
        _FresnelScale("Rim Scale",range(0,1)) = 0
        _FresnelPower("Rim Power",float) = 2
        _FresnelColor("Rim Color",color) = (0,0,0,0)
        
        [Toggle(EMISSION)]_EmissionEnable("Emission",float) = 0
        [HDR]_Emission("Emission",color) = (0,0,0,0)
        _EmissionTex("Emission Texture",2D) = "black"{}
        
        [Toggle(FOG)]_FogEnable("Fog",float) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "RenderPipeline" = "LightweightPipeline"}
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma shader_feature FOG
            #pragma shader_feature EMISSION
            #pragma shader_feature RIM
            #pragma multi_compile_fog
            
            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
                float3 pos : TEXCOORD2;
                float3 normal : TEXCOORD3;
            };

            CBUFFER_START(UnityPerMaterial)

            sampler2D _MainTex;
            float4 _MainTex_ST;
            
            fixed _CutOff;
            
            fixed4 _Color;

            fixed _FresnelScale;
            fixed _FresnelPower;
            fixed4 _FresnelColor;

            fixed4 _Emission;
            sampler2D _EmissionTex;
            float4 _EmissionTex_ST;

            CBUFFER_END

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.normal = UnityObjectToWorldNormal(v.normal).xyz;
                o.pos = mul(unity_ObjectToWorld,v.vertex).xyz;
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_MainTex, i.uv) * _Color;
                
                clip(col.a - _CutOff);
                
                fixed4 fresnelcol = fixed4(0,0,0,0);
                
                #if RIM
                fixed3 viewDir = normalize(UnityWorldSpaceViewDir(i.pos)).xyz;
                fixed3 normalDir = normalize(i.normal);
                fixed fresnel =  _FresnelScale + (1- _FresnelScale) * pow(1-dot(viewDir,normalDir),_FresnelPower);
                fresnelcol = fresnel * _FresnelColor;
                #endif
                
                #if EMISSION
                fixed4 emission = tex2D(_EmissionTex,i.uv) * _Emission;
                col += emission;
                #endif
                
                #if FOG
                UNITY_APPLY_FOG(i.fogCoord, col);
                #endif
                
                return fixed4(col.rgb,_Color.a) + fresnelcol;
            }
            ENDCG
        }
        
    }
}
