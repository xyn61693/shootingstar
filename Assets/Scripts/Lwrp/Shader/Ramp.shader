﻿Shader "Pero/Ramp"
{
    Properties
    {
		MainTex("Texture", 2D) = "white" {}
		_Color("Color",color) = (1,1,1,1)
		
		[Toggle(RECEIVE_SHADOW)] _ReceiveShadow("Receive Shadow",float) = 0
		_ShadowOffset("Shadow Offset",range(0,1)) = 0.5
        _ShadowColor("Shadow Color",color) = (0,0,0,0)
        
        [Toggle(RIM)]_FresnelEnable("Rim",float) = 0
        _FresnelScale("Rim Scale",range(0,1)) = 0
        _FresnelPower("Rim Power",float) = 2
        _FresnelColor("Rim Color",color) = (0,0,0,0)
        
        [Toggle(EMISSION)]_EmissionEnable("Emission",float) = 0
        [HDR]_Emission("Emission",color) = (0,0,0,0)
        _EmissionTex("Emission Texture",2D) = "black"{}
        
        [Toggle(FOG)]_FogEnable("Fog",float) = 0
    }
    
    SubShader
    {
        Tags { "RenderPipeline"="LightweightPipeline" "RenderType"="Opaque" "Queue"="Geometry" }
		Cull Back
		HLSLINCLUDE
		#pragma target 2.0
		ENDHLSL
		
        Pass
        {
        	Tags {"LightMode"="LightweightForward"}
        	Name "Base"
			Blend One Zero
			//ZWrite On
			//ZTest LEqual
			//Offset 0 , 0
			//ColorMask RGBA
            
        	HLSLPROGRAM
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x

        	// -------------------------------------
            // Lightweight Pipeline keywords
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            //#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
            //#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile _ _SHADOWS_SOFT
            #pragma multi_compile _ _MIXED_LIGHTING_SUBTRACTIVE
            
        	// -------------------------------------
            // Unity defined keywords
            #pragma multi_compile _ DIRLIGHTMAP_COMBINED
            #pragma multi_compile _ LIGHTMAP_ON
            #pragma multi_compile_fog

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

        	#include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/Core.hlsl"
        	#include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/Lighting.hlsl"

            #pragma vertex vert
        	#pragma fragment frag
            
            #pragma shader_feature RIM
            #pragma shader_feature EMISSION
            #pragma shader_feature FOG
            #pragma shader_feature RECEIVE_SHADOW
            
			sampler2D MainTex;
			sampler2D _EmissionTex;
			
			CBUFFER_START(UnityPerMaterial)
			half3 _Color;
			half4 MainTex_ST;

            half _ShadowOffset;
            half4 _ShadowColor;
            
            half _FresnelScale;
            half _FresnelPower;
            half4 _FresnelColor;
            
            half4 _Emission;
            float4 _EmissionTex_ST;
            CBUFFER_END
            
            struct GraphVertexInput
            {
                float4 vertex       : POSITION;
                float3 normal       : NORMAL;
                float4 tangent      : TANGENT;
                float4 texcoord     : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

        	struct GraphVertexOutput
            {
                float4 clipPos                  : SV_POSITION;
                float4 lightmapUVOrVertexSH	    : TEXCOORD0;
        		half4 fogFactorAndVertexLight   : TEXCOORD1; // x: fogFactor, yzw: vertex light
            	float4 shadowCoord              : TEXCOORD2;
            	
            	float3 normal                   : TEXCOORD3;
            	float3 worldPos                   : TEXCOORD4;
				float4 texcoord                 : TEXCOORD7;
				
                UNITY_VERTEX_INPUT_INSTANCE_ID
            	UNITY_VERTEX_OUTPUT_STEREO
            };

			half4 LightweightFragmentRamp(InputData inputData, half3 Albedo, half3 Emission, half Alpha)
            {
                Light mainLight = GetMainLight(inputData.shadowCoord);
                half3 attenuatedLightColor = mainLight.color * (mainLight.distanceAttenuation * mainLight.shadowAttenuation);
                half3 lambert = (dot(inputData.normalWS,mainLight.direction) * 0.5 + 0.5);
                
                /*
                #ifdef _ADDITIONAL_LIGHTS
                int pixelLightCount = GetAdditionalLightsCount();
                for (int i = 0; i < pixelLightCount; ++i)
                {
                    Light light = GetAdditionalLight(i, inputData.positionWS);
                    half3 attenuatedLightColor = light.color * (light.distanceAttenuation * light.shadowAttenuation);
                    diffuseColor += LightingLambert(attenuatedLightColor, light.direction, inputData.normalWS);
                }
                #endif
                
                #ifdef _ADDITIONAL_LIGHTS_VERTEX
                diffuseColor += inputData.vertexLighting;
                #endif
                */
                
                half ramp = step(_ShadowOffset,lambert).x;
                
                #if RECEIVE_SHADOW
                ramp = ramp * step(0.9,attenuatedLightColor).x;
                #endif
                
                half3 shadow = (1 - ramp) * (1-_ShadowColor.xyz) * _ShadowColor.a;
                half3 finalColor = Albedo * _Color - shadow;
                
                #if RIM
                half fresnel = _FresnelScale + (1- _FresnelScale) * pow(1-dot(inputData.viewDirectionWS,inputData.normalWS),_FresnelPower);
                half3 fresnelcol = fresnel * _FresnelColor;
                finalColor += fresnelcol;
                #endif
                
                #if EMISSION
                finalColor += Emission;
                #endif
                
                #if FOG
                finalColor.rgb = MixFog(finalColor.rgb, inputData.fogCoord);
                #endif
                
                return half4(finalColor,Alpha);
            }
			
            GraphVertexOutput vert (GraphVertexInput v )
        	{
        		GraphVertexOutput o = (GraphVertexOutput)0;
                UNITY_SETUP_INSTANCE_ID(v);
            	UNITY_TRANSFER_INSTANCE_ID(v, o);
        		UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

                o.texcoord.xy = v.texcoord.xy;
                o.texcoord.zw = 0;
                
                o.normal = TransformObjectToWorldNormal(v.normal);
				o.worldPos = TransformObjectToWorld(v.vertex.xyz);
                VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex.xyz);
                
        	    OUTPUT_LIGHTMAP_UV(v.texcoord, unity_LightmapST, o.lightmapUVOrVertexSH.xy);
                OUTPUT_SH(o.worldPos,o.lightmapUVOrVertexSH.xyz);
                
        	    half3 vertexLight = VertexLighting(vertexInput.positionWS, o.normal);
        	    half fogFactor = ComputeFogFactor(vertexInput.positionCS.z);
        	    
        	    o.fogFactorAndVertexLight = half4(fogFactor, vertexLight);
        	    o.clipPos = vertexInput.positionCS;
        		o.shadowCoord = GetShadowCoord(vertexInput);
        		
        		return o;
        	}

        	half4 frag (GraphVertexOutput IN) : SV_Target
            {
            	UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(IN);
                
				float3 WorldSpaceViewDirection = SafeNormalize( _WorldSpaceCameraPos.xyz - IN.worldPos);
				float2 uvMainTex = IN.texcoord.xy * MainTex_ST.xy + MainTex_ST.zw;

        		InputData inputData;
        		inputData.positionWS = IN.worldPos;
        	    inputData.normalWS = normalize(IN.normal);
        	    inputData.viewDirectionWS = normalize(WorldSpaceViewDirection);

        	    inputData.shadowCoord = IN.shadowCoord;
        	    inputData.fogCoord = IN.fogFactorAndVertexLight.x;
        	    inputData.vertexLighting = IN.fogFactorAndVertexLight.yzw;
        	    inputData.bakedGI = SAMPLE_GI(IN.lightmapUVOrVertexSH.xy, IN.lightmapUVOrVertexSH.xyz, inputData.normalWS);

                float3 Albedo = tex2D( MainTex, uvMainTex ).rgb;
                float3 Emission = tex2D(_EmissionTex,uvMainTex).rgb * _Emission.rbg;
				float Alpha = 1;

        		return LightweightFragmentRamp(inputData,Albedo,Emission,Alpha);
            }
        	ENDHLSL
        }
		
        Pass
        {
        	Name "ShadowCaster"
            Tags { "LightMode"="ShadowCaster" }
			//ZWrite On
			//ZTest LEqual

            HLSLPROGRAM
            // Required to compile gles 2.0 with standard srp library
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

            #pragma vertex ShadowPassVertex
            #pragma fragment ShadowPassFragment

            #include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/Lighting.hlsl"
            //#include "Packages/com.unity.render-pipelines.lightweight/ShaderLibrary/ShaderGraphFunctions.hlsl"
            //#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

            struct GraphVertexInput
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };
			
        	struct VertexOutput
        	{
        	    float4 clipPos      : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
        	};
			
            float3 _LightDirection;

            VertexOutput ShadowPassVertex(GraphVertexInput v )
        	{
        	    VertexOutput o;
        	    UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o);

        	    float3 positionWS = TransformObjectToWorld(v.vertex.xyz);
                float3 normalWS = TransformObjectToWorldDir(v.normal);
                float invNdotL = 1.0 - saturate(dot(_LightDirection, normalWS));
                float scale = invNdotL * _ShadowBias.y;

                positionWS = _LightDirection * _ShadowBias.xxx + positionWS;
				positionWS = normalWS * scale.xxx + positionWS;
                float4 clipPos = TransformWorldToHClip(positionWS);

        	#if UNITY_REVERSED_Z
        	    clipPos.z = min(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
        	#else
        	    clipPos.z = max(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
        	#endif
                o.clipPos = clipPos;
                
        	    return o;
        	}

            half4 ShadowPassFragment(VertexOutput IN  ) : SV_TARGET
            {
                UNITY_SETUP_INSTANCE_ID(IN);
                return 0;
            }
            ENDHLSL
        }
    }
    Fallback "Hidden/InternalErrorShader"
}
