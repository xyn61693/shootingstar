﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private Rigidbody m_Rigidbody;
    private TrailRenderer m_TrailRenderer;
//    private Collider m_Collider;
    
    private float m_Distance;

    private bool m_IsFlying = false;
    
    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_TrailRenderer = GetComponent<TrailRenderer>();
//        m_Collider = GetComponent<Collider>();
       m_TrailRenderer.enabled = false;
    }

    private float m_Time;

    private void FixedUpdate()
    {
        if(CharacterManager.instance.player == null) return;
        m_Distance = Vector3.Distance(transform.position, CharacterManager.instance.player.transform.position);
        if (m_Distance <= 3f || m_IsFlying)
        {
//            if (!m_Collider.enabled) m_Collider.enabled = false;
            if (!m_TrailRenderer.enabled) m_TrailRenderer.enabled = true;
            m_Rigidbody.velocity = (CharacterManager.instance.player.transform.position - transform.position) * 10f;
        }
        if (m_Distance <= 0.3f)
        {
            m_IsFlying = false;
            m_TrailRenderer.Clear();
            m_TrailRenderer.enabled = false;
//            m_Collider.enabled = true;
            PoolManager.instance.Recycle(gameObject);
            RewardManager.ObtainCoin(1,this);
        }
    }

    public void Fly()
    {
        m_IsFlying = true;
    }

    public void Fly(float time)
    {
        Invoke("Fly",time);        
    }
}
