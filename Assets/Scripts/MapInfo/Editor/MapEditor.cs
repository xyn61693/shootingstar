﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Schema;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityEngine;
using Random = System.Random;

public class MapEditor : OdinEditorWindow
{
    [MenuItem("Tools/MapEditor2")]
    public static void OpenWindow()
    {
        GetWindow<MapEditor>().Show();
    }

    [OnValueChanged("OnAssetChange")]public MapInfoAsset asset;

    private void OnAssetChange()
    {
        OnDestroy();
        OnEnable();
        LoadAsset();
    }
    
    private GameObject m_Map;

    private GameObject m_Plane;
    
    private List<CubeInfo> m_Infos = new List<CubeInfo>();

    private bool m_AllowPaint;

    protected override void OnEnable()
    {
        SceneView.duringSceneGui -= OnSceneView;
        SceneView.duringSceneGui += OnSceneView;
        
        m_Map = GameObject.Find("Map");
        if (m_Map == null)
        {
            m_Map = new GameObject {name = "Map"};
        }

        m_Plane = GameObject.Find("Plane");
        if (m_Plane == null)
        {
            m_Plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
        }
    }

    protected override void OnDestroy()
    {
        SceneView.duringSceneGui -= OnSceneView;
        DestroyImmediate(m_Plane);
        DestroyImmediate(m_Map);
    }

    private void LoadAsset()
    {
        if (!asset) return;

        size = asset.mapSize;
        ChangeSize();
        
        m_Infos.Clear();

        if (asset.map.Count > 0)
        {
            var key = asset.map.Keys.ToList();
            for (int i = 0; i < key.Count; i++)
            {
                var pos = key[i];
                var type = asset.map[pos];
                Create(pos, type);
            }
        }

        if (asset.enemy.Count > 0)
        {
            var key = asset.enemy.Keys.ToList();
            for (int i = 0; i < key.Count; i++)
            {
                var pos = key[i];
                var type = asset.enemy[pos];
                Create(pos,type);
            }
        }
    }

    [InlineButton("ChangeSize","Chagne")]public Vector2Int size;

    private void ChangeSize()
    {
        m_Plane.transform.localScale = new Vector3(size.x*0.1f,1,size.y*0.1f);
        var offsetX = size.x % 2 > 0 ? 0.5f : 0;
        var offsetY = size.y % 2 > 0 ? 0.5f : 0;
        m_Plane.transform.position = Vector3.zero + new Vector3(offsetX, 0,offsetY);
        asset.mapSize = size;        
        m_IsChange = true;
    }
    
    public enum BrushMode
    {
        Map,
        Enemy
    }
    
    [EnumToggleButtons]public BrushMode brushMode;

    public bool isRandomBrush;
    
    public GameObject mapBrush;

    [ShowIf("isRandomBrush")]public List<GameObject> mapBrushRandomList = new List<GameObject>();
    
    public GameObject enemyBrush;
    
    [ShowIf("isRandomBrush")]public List<GameObject> enemyBrushRandomList = new List<GameObject>();

    public int layer;

    [Button("Fill")]
    private void FillMap()
    {
        var brush = brushMode == BrushMode.Map ? mapBrush : enemyBrush;
        var x = Mathf.CeilToInt(size.x / 2f) + (size.x % 2 == 0 ? -0.5f : -1.5f);
        var y = Mathf.CeilToInt(size.y / 2f) + (size.y % 2 == 0 ? -0.5f : -1.5f);
        for (int i = 0; i < size.x; i++)
        {
            for (int j = 0; j < size.y; j++)
            {
                var pos = new Vector3(i - x, layer, j - y);
                var cube = Instantiate(brush,pos,Quaternion.identity);
                cube.transform.SetParent(m_Map.transform);
                cube.name = brush.name;
                m_Infos.Add(new CubeInfo(){obj = cube,center = pos,mode = brushMode});
            }
        }
    }
    
    private bool m_SetPlayer;
    
    [Button("Set Player")]
    private void SetPlayerPos()
    {
        m_SetPlayer = true;
        m_SetAltar = false;
    }

    private bool m_SetAltar;

    [Button("Set Altar")]
    private void SetAltarPos()
    {
        m_SetAltar = true;
        m_SetPlayer = false;
    }
    
    private bool m_IsChange;
    
    [EnableIf("m_IsChange"),Button("Save",30)]
    private void SaveAsset()
    {
        asset.mapSize = new Vector2Int(size.x, size.y);
        
        asset.map.Clear();
        asset.enemy.Clear();
        
        for (int i = 0; i < m_Infos.Count; i++)
        {
            if (m_Infos[i].mode == BrushMode.Map)
            {
                MapInfo m; 
                MapInfo.TryParse(m_Infos[i].obj.name,out m);
                asset.map.Add(m_Infos[i].center,m);
            }
            else if (m_Infos[i].mode == BrushMode.Enemy)
            {
                EnemyType m; 
                EnemyType.TryParse(m_Infos[i].obj.name,out m);
                asset.enemy.Add(m_Infos[i].center,m);
            }
        }

        m_IsChange = false;
        AssetDatabase.SaveAssets();
    }
    
    private Vector3 m_MousePos;

    private void OnSceneView(SceneView view)
    {
        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        Selection.activeGameObject = null;

        var e = GetUserInput();
        if (e != null && m_AllowPaint)
        {
            if(m_SetPlayer) Handles.color = Color.blue;
            if(m_SetAltar) Handles.color = Color.green;
            else Handles.color = e.control ? Color.red : Color.white;
            Handles.DrawWireCube(m_MousePos, Vector3.one);
        }

        if (asset != null)
        {
            if (!m_SetPlayer)
            {
                Handles.color = Color.blue;
                Handles.Label(asset.player, "Player");
                Handles.DrawWireCube(asset.player, Vector3.one);
            }

            if (!m_SetAltar)
            {
                Handles.color = Color.green;
                Handles.Label(asset.altar, "Altar");
                Handles.DrawWireCube(asset.altar, Vector3.one);
            }
        }
    }

    private Event GetUserInput()
    {
        var e = Event.current;
        if(e == null)return null;
        m_MousePos = GetMousePoint(e);
        GetKeyInput(e);
        return e;
    }

    private Vector3 GetMousePoint(Event e)
    {
        var ray = HandleUtility.GUIPointToWorldRay(e.mousePosition);
        RaycastHit hitinfo;
        m_AllowPaint = Physics.Raycast(ray,out hitinfo);
        return m_AllowPaint
            ? new Vector3(Mathf.CeilToInt(hitinfo.point.x) - 0.5f, layer/* + 0.5f*/,
                Mathf.CeilToInt(hitinfo.point.z) - 0.5f)
            : Vector3.zero;
    }

    private void GetKeyInput(Event e)
    {
        if(!m_AllowPaint) return;
        if ((e.type == EventType.MouseDrag || e.type == EventType.MouseDown) && !e.alt)
        {
            if (e.button == 0)
            {
                m_IsChange = true;
                if (m_SetPlayer)
                {
                    asset.player =  m_MousePos;
                    m_SetPlayer = false;
                }
                else if (m_SetAltar)
                {
                    asset.altar = m_MousePos;
                    m_SetAltar = false;
                }
                else
                {
                    if (e.control) Delete();
                    else Create();
                }
            }
        }
    }

    private void Create()
    {
        GameObject brush = null;
        if (isRandomBrush)
        {
            var list = brushMode == BrushMode.Map ? mapBrushRandomList : enemyBrushRandomList;
            brush = list[UnityEngine.Random.Range(0, list.Count)];
        }
        else
        {
            brush = brushMode == BrushMode.Map ? mapBrush : enemyBrush;
        }
        if (brush == null || m_MousePos == Vector3.zero) return;
        for (int i = 0; i < m_Infos.Count; i++)
        {
            if(m_Infos[i].center == m_MousePos) return;
        }
        var cube = Instantiate(brush,m_MousePos,Quaternion.identity);
        cube.transform.SetParent(m_Map.transform);
        cube.name = brush.name;
        m_Infos.Add(new CubeInfo(){obj = cube,center = m_MousePos,mode = brushMode});
    }

    private void Create(Vector3 pos,MapInfo info)
    {
#if UNITY_EDITOR
        var guid = AssetDatabase.FindAssets("t: GameObject " +info);
        if (guid.Length == 0) return;
        var path = AssetDatabase.GUIDToAssetPath(guid[0]);
        var obj = AssetDatabase.LoadAssetAtPath<GameObject>(path);
        var cube = Instantiate(obj,pos,Quaternion.identity);
        cube.transform.SetParent(m_Map.transform);
        cube.name = info.ToString();
        m_Infos.Add(new CubeInfo(){obj = cube,center = pos,mode = BrushMode.Map});
#endif
    }
    
    private void Create(Vector3 pos,EnemyType type)
    {
#if UNITY_EDITOR
        var guid = AssetDatabase.FindAssets("t: GameObject " +type);
        if (guid.Length == 0) return;
        var path = AssetDatabase.GUIDToAssetPath(guid[0]);
        var obj = AssetDatabase.LoadAssetAtPath<GameObject>(path);
        var cube = Instantiate(obj,pos,Quaternion.identity);
        cube.transform.SetParent(m_Map.transform);
        cube.name = type.ToString();
        m_Infos.Add(new CubeInfo(){obj = cube,center = pos,mode = BrushMode.Enemy});
#endif
    }

    private void Delete()
    {
        for (int i = 0; i < m_Infos.Count; i++)
        {
            if (m_Infos[i].center == m_MousePos)
            {
                DestroyImmediate(m_Infos[i].obj);
                m_Infos.Remove(m_Infos[i]);
                return;
            }
        }
    }

    private void Optimization()
    {
    }
    
    public struct CubeInfo
    {
        public GameObject obj;
        public BrushMode mode;
        public Vector3 center;
    }
}
