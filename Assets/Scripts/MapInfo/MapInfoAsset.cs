﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName ="MapInfoAsset.asset",menuName = "MapInfoAsset")]
public class MapInfoAsset : SerializedScriptableObject
{
   public MapType type;
   
   public Vector2Int mapSize;

   public Vector3 player;

   public Vector3 altar;
   
   public Dictionary<Vector3,MapInfo> map = new Dictionary<Vector3, MapInfo>();
   
   public Dictionary<Vector3,EnemyType> enemy = new Dictionary<Vector3, EnemyType>();
}

public enum MapType
{
   Normal,
   Boss
}

public enum MapInfo
{
   Wall = 0,
   Water = 1,
   Stab = 2,
   Ground = 3
}

public enum EnemyType
{
   None = 0,
   Skull = 1,
   Bat = 2,
   Scorpion = 3,
   SkullArcher = 4,
   BossBat = 100,
}
