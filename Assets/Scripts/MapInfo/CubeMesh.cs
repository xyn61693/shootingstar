﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer))]
public class CubeMesh : MonoBehaviour
{    
    private Vector3[] m_CubeVertex = { 
        //上面四个顶点
        new Vector3(-0.5f,0.5f,0.5f),//左上
        new Vector3(0.5f,0.5f,0.5f),//右上
        new Vector3 (0.5f,0.5f,-0.5f),//右下
        new Vector3(-0.5f,0.5f,-0.5f),//左下
        //下面四个顶点
        new Vector3(-0.5f,-0.5f,0.5f),//左上
        new Vector3(0.5f,-0.5f,0.5f),//右上
        new Vector3(0.5f,-0.5f,-0.5f),//右下
        new Vector3(-0.5f,-0.5f,-0.5f) //左下
    };

    private enum CubeSurface
    {
        Front,
        Right,
        Back,
        Left,
        Up,
        Down
    }

    private List<Vector3> m_Vertices = new List<Vector3>();
    
    private List<int> m_Triangles = new List<int>();

    private List<Vector3> m_Center = new List<Vector3>();
    
    public void AddMesh(Vector3 center)
    {
        if (m_Center.Contains(center)) return;
        m_Center.Add(center);
        RefreshMesh();
    }

    public void DeleteMesh(Vector3 center)
    {
        if (m_Center.Contains(center))
        {
            m_Center.Remove(center);
        }
        RefreshMesh();
    }

    #region Fun
    private Vector3[] GetCubeVertex(Vector3 center)
    {
        return new []
        {
            m_CubeVertex[0] + center,
            m_CubeVertex[1] + center,
            m_CubeVertex[2] + center,
            m_CubeVertex[3] + center,
            m_CubeVertex[4] + center,
            m_CubeVertex[5] + center,
            m_CubeVertex[6] + center,
            m_CubeVertex[7] + center,
        };
    }
    
    private void AddCubeSurface(Vector3[] vertex,CubeSurface suface,out Vector3[] v ,out int[] t)
    {
        v = null;
        switch (suface)
        {
            case CubeSurface.Up:
                v = new[] {vertex[0], vertex[1], vertex[2], vertex[3]};
                break;
            case CubeSurface.Down:
                v = new[] {vertex[5], vertex[4], vertex[7], vertex[6]};
                break;
            case CubeSurface.Left:
                v = new[] {vertex[0], vertex[3], vertex[7], vertex[4]};
                break;
            case CubeSurface.Right:
                v = new[] {vertex[2], vertex[1], vertex[5], vertex[6]};
                break;
            case CubeSurface.Front:
                v = new[] {vertex[1], vertex[0], vertex[4], vertex[5]};
                break;
            case CubeSurface.Back:
                v = new[] {vertex[3], vertex[2], vertex[6], vertex[7]};
                break;
        }
        int index = m_Vertices.Count;
        t = new[] {index, index + 1, index + 2, index, index + 2, index + 3};
    }

    private void RefreshMesh()
    {
        InitMesh();
        for (int i = 0; i < m_Center.Count; i++)
        {
            var v = GetCubeVertex(m_Center[i]);
            for (int j = 0; j < 6; j++)
            {
                Vector3[] vertex;
                int[] triangle;
                AddCubeSurface(v, (CubeSurface) j,out vertex,out triangle);
                m_Vertices.AddRange(vertex);
                m_Triangles.AddRange(triangle);
            }
        }
        ApplyMesh();
    }
    
    private void InitMesh()
    {
        var m = GetComponent<MeshFilter>().sharedMesh;
        if (m == null)
        {
            GetComponent<MeshFilter>().sharedMesh = new Mesh();
            m_Center.Clear();
        }
        m_Vertices.Clear();
        m_Triangles.Clear();
    }
    
    private void ApplyMesh()
    {
        var m = GetComponent<MeshFilter>().sharedMesh = new Mesh();
        m.SetVertices(m_Vertices);
        m.SetTriangles(m_Triangles, 0);
        m.RecalculateNormals();
    }
    
    #endregion
}
