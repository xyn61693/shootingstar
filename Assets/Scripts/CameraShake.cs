﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    private static CameraShake m_Instance;
	
    public static CameraShake instance
    {
        get { return m_Instance; }
    }

    private Coroutine m_ShakeCoroutine;

    private Vector3 m_ShakePos;
    
    private void Awake()
    {
        m_Instance = this;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.J))
        {
            ShakeCamera();
        }
    }
    
    public void ShakeCamera()
    {
        if (m_ShakeCoroutine != null)
        {
            StopCoroutine(m_ShakeCoroutine);
            transform.localPosition = Vector3.zero;
        }
        m_ShakeCoroutine = StartCoroutine(Shake());
    }
    
    private IEnumerator Shake()
    {
        yield return ShakePass(0.125f);
        yield return ShakePass(0.1f);
        transform.localPosition = Vector3.zero;
        m_ShakeCoroutine = null;
    }
    
    private IEnumerator ShakePass(float value)
    {
        m_ShakePos = Random.insideUnitSphere * value;
        transform.localPosition += m_ShakePos;
        yield return null;
        transform.localPosition -= m_ShakePos;
        yield return null;
    }
}
