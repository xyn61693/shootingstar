﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class Altar : SerializedMonoBehaviour
{
    public RewardManager.AltarType type;

    public RandomSeedAsset itemLevel;

    public Dictionary<RewardManager.ItemLevel,RandomSeedAsset> item = new Dictionary<RewardManager.ItemLevel, RandomSeedAsset>();
    
    private bool m_IsActive = true;
    
    private string m_Item;

    private string m_Message;

    private Animator m_Animator;
    
    private RewardManager.ItemLevel m_Level;

    private void Awake()
    {
        m_Animator = GetComponentInChildren<Animator>();
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (m_IsActive && other.CompareTag("Player"))
        {
            Interactive();
        }
    }

    private void Interactive()
    {
        if (type == RewardManager.AltarType.Free)
        {
//            MessageManager.instance.Show("Free",ActiveAltar);
            ActiveAltar();
        }
        else
        {
            if (type == RewardManager.AltarType.Coin)
            {
                m_Message = string.Concat("Cost ", RewardManager.currentCoinAltarPrice, " coin to get item");
            }
            else if (type == RewardManager.AltarType.Hp)
            {
                m_Message = "Cost 25% HP to get item";
            }
            else if(type == RewardManager.AltarType.Life)
            {
                m_Message = "Reduce the 10% maximum health to get item";
            }
            else if (type == RewardManager.AltarType.Challenge)
            {
                m_Message = "Finish a challenge to get item";
            }

            MessageManager.instance.ShowPanelWithOption(m_Message, b =>
            {
                if (type == RewardManager.AltarType.Coin)
                {
                    if (!b) return;
                    if (RewardManager.ConsumerCoin())
                    {
                        ActiveAltar();
                    }
                    else
                    {
                        MessageManager.instance.ShowPanel("Not enough coin");
                    }
                }
                else
                {
                    if (b) ActiveAltar();
                }
            });
        }
    }

    private void ActiveAltar()
    {
        m_IsActive = false;
        m_Animator.SetBool("Open",true);
        if (type == RewardManager.AltarType.Free || type == RewardManager.AltarType.Coin)
        {
            ApplyItem();
        }
        else if(type == RewardManager.AltarType.Hp)
        {
            CharacterManager.instance.player.ReduceHp(0.25f);
            ApplyItem();
        }
        else if(type == RewardManager.AltarType.Life)
        {
            CharacterManager.instance.player.ReduceMaxHp(10);
            ApplyItem();
        }
        else if(type == RewardManager.AltarType.Challenge)
        {
            Debug.Log("Start Challenge");
        }
    }

    private void ApplyItem()
    {
        Enum.TryParse(RandomUtils.RandomSeed(itemLevel), out m_Level);
        if (m_Level == RewardManager.ItemLevel.None)
        {
            Debug.Log("Get level None");
            return;
        }
        m_Item = RandomUtils.RandomSeed(item[m_Level]);
        CharacterManager.instance.player.GetComponent<CharacterItemController>().ShowItem(m_Item);
    }

    public void Recycle()
    {
        m_IsActive = true;
        m_Animator.SetBool("Open",false);
        m_Animator.Play("Close");
        PoolManager.instance.Recycle(gameObject);
    }
}
