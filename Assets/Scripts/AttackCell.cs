﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackCell : MonoBehaviour
{
    protected float m_Speed;

    protected int m_Damage;

    public int reflectTime;
    private int m_ReflectTime;
    
    private bool m_IsCrit;

    private Rigidbody m_Rigidbody;
    
    public bool isPalyer;
    public bool isEnemy;

    public GameObject hurt;

    public GameObject destroyEffect;

    private int m_CrowbarBonus;
    
    private int m_AxeBonus;
    
    private Action<AiController,bool> m_IsHitEnemyCb;

    private Action<AiController> m_IsKillEnemyCb;

    private bool m_IsShowText;
    
//    private Coroutine m_LifeTimeCoroutine;

    private TrailRenderer m_TrailRenderer;

    private void Awake()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Rigidbody.mass = 0;
        GetComponent<Collider>().isTrigger = false;
        m_TrailRenderer = GetComponent<TrailRenderer>();
    }
    
    public void Init(float speed,int damage,bool isCrit,int crowbarBonus,int axeBonus,Action<AiController,bool> isHitEnemy,Action<AiController> isKillEnemy,bool showText = true)
    {
        m_Speed = speed;
        m_Damage = damage;
        m_IsCrit = isCrit;
        m_CrowbarBonus = crowbarBonus;
        m_AxeBonus = axeBonus;
        m_IsHitEnemyCb = isHitEnemy;
        m_IsKillEnemyCb = isKillEnemy;
        m_IsShowText = showText;
        
//        m_LifeTimeCoroutine = StartCoroutine(LifeTimeDestory());
        m_Rigidbody.velocity = transform.forward * m_Speed;
    }
/*
    private IEnumerator LifeTimeDestory()
    {
        yield return new WaitForSeconds(5f);
        Debug.Log("Destory LifeTime");
        Destroy();
    }
*/
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            m_ReflectTime++;
            if (m_ReflectTime >= reflectTime)
            {
                PlayDestroyEffect(other.transform,other.contacts[0].point,other.contacts[0].normal);
                Destroy();
            }
            else
            {
                transform.rotation = Quaternion.LookRotation(Reflect(other.contacts[0].normal));
                m_Rigidbody.velocity = transform.forward * m_Speed;
            }
        }
        else if (other.gameObject.CompareTag("Enemy") && !isEnemy)
        {
            var isDead = OnHitEnemy(other.gameObject);
            if (!isDead)
            {
                PlayDestroyEffect(other.transform,other.contacts[0].point,other.contacts[0].normal);
            }
            Destroy();
        }
        else if (other.gameObject.CompareTag("Player") && !isPalyer)
        {
            OnHitPlayer(other.gameObject);
            PlayDestroyEffect(other.transform,other.contacts[0].point,other.contacts[0].normal);
            Destroy();
        }
        else
        {
            Debug.Log("Hit "+ other.gameObject.name);
        }
    }

    protected virtual bool OnHitEnemy(GameObject obj)
    {
        var ai = obj.GetComponent<AiController>();
        AddHitBonus(ai);
        var isDead = ai.Injured(m_Damage);
        if(!isDead && m_IsHitEnemyCb != null) m_IsHitEnemyCb.Invoke(ai,m_IsCrit);
        if(isDead && m_IsKillEnemyCb != null) m_IsKillEnemyCb.Invoke(ai);
        PlayHurtTextEffect();
        return isDead;
    }

    private void AddHitBonus(AiController enemy)
    {
        if (m_CrowbarBonus > 0f && enemy.GetHpPercent() >= 0.8f)
        {
            m_Damage += Mathf.CeilToInt((m_Damage * m_CrowbarBonus) * 0.01f);
        }

        if (m_AxeBonus > 0f && enemy.GetHpPercent() <= 0.3f)
        {
            m_Damage += Mathf.CeilToInt((m_Damage * m_AxeBonus) * 0.01f);
        }
    }
    
    protected virtual void OnHitPlayer(GameObject obj)
    {
        obj.GetComponent<CharacterHealthyController>().Injured(m_Damage);
    }

    private void PlayHurtTextEffect()
    {
        if (!m_IsShowText) return;
        var h = PoolManager.instance.GetCell(hurt);
        h.GetComponent<TxtMessage>().SetDamage(m_Damage, m_IsCrit, transform.position);
    }

    private void Destroy()
    {
/*
        if (m_LifeTimeCoroutine != null)
        {
            StopCoroutine(m_LifeTimeCoroutine);
            m_LifeTimeCoroutine = null;
        }
*/
        if (m_TrailRenderer)
        {
            m_TrailRenderer.Clear();
        }
        m_ReflectTime = 0;
        PoolManager.instance.Recycle(gameObject);
    }
    
    private void PlayDestroyEffect(Transform hit,Vector3 pos,Vector3 normal)
    {
        if (destroyEffect)
        {
            var d = PoolManager.instance.GetCell(destroyEffect);
            //d.transform.SetParent(hit);
            d.transform.position = pos;
//            d.transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles + new Vector3(0, 180, 0));
            d.transform.rotation = Quaternion.LookRotation(normal);
            d.transform.localScale = Vector3.one;
            d.GetComponent<DestroyEffect>().Play();
        }
    }

    private Vector3 Reflect(Vector3 normal)
    {
        var f = transform.forward;
        if (Math.Abs(f.x) < 0.01f)
        {
            return new Vector3(0, f.y, -f.z);
        }
        if (Math.Abs(f.z) < 0.01f)
        {
            return new Vector3(-f.x, f.y, 0);
        }
        if (f.x > 0f)
        {
            if (f.z > 0f)
            {
                return normal == Vector3.left ? new Vector3(-f.x, f.y, f.z) : new Vector3(f.x, f.y, -f.z);
            }
            return normal == Vector3.left ? new Vector3(-f.x, f.y, f.z) : new Vector3(f.x, f.y, -f.z);
        }
        else
        {
            if (f.z > 0f)
            {
                return normal == Vector3.right ? new Vector3(-f.x, f.y, f.z) : new Vector3(f.x, f.y, -f.z);
            }
            return normal == Vector3.right ? new Vector3(-f.x, f.y, f.z) : new Vector3(f.x, f.y, -f.z);
        }
    }
}
